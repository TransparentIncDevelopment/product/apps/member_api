use member_api::banks::{Bank, BankAdapter, McbAdapterConfig, TreasuryPrimeAdapterConfig};
use url::Url;

pub mod errors;
pub mod routes;

#[cfg(test)]
pub mod tests;

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ApiBank {
    pub id: i32,
    pub name: String,
    pub routing_number: String,
    pub claims_account: String,
    pub adapter: ApiBankAdapter,
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
#[serde(rename_all = "camelCase")]
pub enum ApiBankAdapter {
    Mcb(ApiMcbAdapter),
    TreasuryPrime(ApiTreasuryPrimeAdapter),
}

impl ApiBankAdapter {
    pub fn is_mcb(&self) -> bool {
        matches!(self, ApiBankAdapter::Mcb(_))
    }
    pub fn is_treasury_prime(&self) -> bool {
        matches!(self, ApiBankAdapter::TreasuryPrime(_))
    }
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ApiMcbAdapter {
    #[serde(with = "url_serde")]
    pub url: Url,
    pub secret_user_name: String,
    pub secret_password: String,
    pub secret_client_app_ident: String,
    pub secret_organization_id: String,
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ApiTreasuryPrimeAdapter {
    #[serde(with = "url_serde")]
    pub url: Url,
    pub secret_api_key_id: String,
    pub secret_api_key_value: String,
}

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct NewBank {
    pub name: String,
    pub routing_number: String,
    pub claims_account: String,
    pub adapter: ApiBankAdapter,
}

impl From<NewBank> for Bank {
    fn from(input: NewBank) -> Self {
        Self {
            id: None,
            name: input.name,
            routing_number: input.routing_number,
            claims_account: input.claims_account,
            adapter: input.adapter.into(),
        }
    }
}

impl From<Bank> for NewBank {
    fn from(input: Bank) -> Self {
        Self {
            name: input.name,
            routing_number: input.routing_number,
            claims_account: input.claims_account,
            adapter: input.adapter.into(),
        }
    }
}

#[cfg(test)]
pub fn assert_adapters_eq(input: BankAdapter, output: ApiBankAdapter) {
    match input.clone() {
        crate::banks::BankAdapter::Mcb(expected) => match output {
            ApiBankAdapter::Mcb(actual) => {
                assert_eq!(actual.url, expected.url);
                assert_eq!(
                    actual.secret_client_app_ident,
                    expected.secret_client_app_ident
                );
                assert_eq!(actual.secret_user_name, expected.secret_user_name);
                assert_eq!(
                    actual.secret_organization_id,
                    expected.secret_organization_id
                );
                assert_eq!(actual.secret_password, expected.secret_password);
            }
            ApiBankAdapter::TreasuryPrime(_) => {
                panic!("Expected:{:?}\nActual:{:?}\n", input, output)
            }
        },
        crate::banks::BankAdapter::TreasuryPrime(expected) => match output {
            ApiBankAdapter::Mcb(_) => panic!("Expected:{:?}\nActual:{:?}\n", input, output),
            ApiBankAdapter::TreasuryPrime(actual) => {
                assert_eq!(actual.url, expected.url);
                assert_eq!(actual.secret_api_key_id, expected.secret_api_key_id);
                assert_eq!(actual.secret_api_key_value, expected.secret_api_key_value);
            }
        },
    }
}

impl From<ApiBankAdapter> for BankAdapter {
    fn from(keys: ApiBankAdapter) -> Self {
        match keys {
            ApiBankAdapter::Mcb(mcb_keys) => BankAdapter::Mcb(mcb_keys.into()),
            ApiBankAdapter::TreasuryPrime(tp_keys) => BankAdapter::TreasuryPrime(tp_keys.into()),
        }
    }
}

impl From<ApiTreasuryPrimeAdapter> for TreasuryPrimeAdapterConfig {
    fn from(keys: ApiTreasuryPrimeAdapter) -> Self {
        TreasuryPrimeAdapterConfig {
            url: keys.url,
            secret_api_key_id: keys.secret_api_key_id,
            secret_api_key_value: keys.secret_api_key_value,
        }
    }
}

impl From<ApiMcbAdapter> for McbAdapterConfig {
    fn from(keys: ApiMcbAdapter) -> Self {
        McbAdapterConfig {
            url: keys.url,
            secret_user_name: keys.secret_user_name,
            secret_password: keys.secret_password,
            secret_client_app_ident: keys.secret_client_app_ident,
            secret_organization_id: keys.secret_organization_id,
        }
    }
}

impl From<Bank> for ApiBank {
    fn from(input: Bank) -> Self {
        ApiBank {
            id: input.id.unwrap_or_default(),
            name: input.name,
            routing_number: input.routing_number,
            claims_account: input.claims_account,
            adapter: input.adapter.into(),
        }
    }
}

impl From<BankAdapter> for ApiBankAdapter {
    fn from(input: BankAdapter) -> ApiBankAdapter {
        match input {
            BankAdapter::Mcb(data) => ApiBankAdapter::Mcb(data.into()),
            BankAdapter::TreasuryPrime(data) => ApiBankAdapter::TreasuryPrime(data.into()),
        }
    }
}

impl From<McbAdapterConfig> for ApiMcbAdapter {
    fn from(input: McbAdapterConfig) -> Self {
        ApiMcbAdapter {
            secret_client_app_ident: input.secret_client_app_ident,
            secret_organization_id: input.secret_organization_id,
            secret_password: input.secret_password,
            secret_user_name: input.secret_user_name,
            url: input.url,
        }
    }
}

impl From<TreasuryPrimeAdapterConfig> for ApiTreasuryPrimeAdapter {
    fn from(input: TreasuryPrimeAdapterConfig) -> Self {
        ApiTreasuryPrimeAdapter {
            secret_api_key_id: input.secret_api_key_id,
            secret_api_key_value: input.secret_api_key_value,
            url: input.url,
        }
    }
}
