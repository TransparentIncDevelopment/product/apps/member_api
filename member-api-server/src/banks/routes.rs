use crate::{
    banks::{errors::create_http_error_response, ApiBank, NewBank},
    error::ErrorContent,
};
use actix_web::{
    web::{scope, Data, Json, Path},
    HttpResponse, Scope,
};
use member_api::{banks::Bank, Error};

use crate::web_server::ServerState;

///
/// Configure routes on an http server.
pub fn routes() -> Scope {
    scope("/api/v1/banks")
        .service(get_banks)
        .service(get_bank)
        .service(post_bank)
        .service(put_bank)
        .service(delete_bank)
}

#[actix_web::get("")]
fn get_banks(state: Data<ServerState>) -> HttpResponse {
    let result = state.service.banks();
    match result {
        Ok(banks) => {
            let accounts: Vec<ApiBank> = banks.into_iter().map(|a| a.into()).collect();
            HttpResponse::Ok().json(&accounts)
        }
        Err(error) => create_banks_error_response(error),
    }
}

fn create_banks_error_response(error: member_api::Error) -> HttpResponse {
    match error {
        Error::BankRepositoryErrors { source } => create_http_error_response(source),
        e => HttpResponse::InternalServerError().json(ErrorContent {
            message: e.to_string(),
        }),
    }
}

#[actix_web::get("/{id}")]
fn get_bank(state: Data<ServerState>, id: Path<i32>) -> HttpResponse {
    let result = state.service.bank(id.into_inner());
    match result {
        Ok(val) => HttpResponse::Ok().json(ApiBank::from(val)),
        Err(error) => create_banks_error_response(error),
    }
}

#[actix_web::post("")]
fn post_bank(state: Data<ServerState>, body: Json<NewBank>) -> HttpResponse {
    let new_bank = body.into_inner();
    let bank = new_bank.into();
    let result = state.service.add_bank(bank);
    match result {
        Ok(val) => HttpResponse::Created().json(ApiBank::from(val)),
        Err(error) => create_banks_error_response(error),
    }
}

#[actix_web::put("/{id}")]
fn put_bank(state: Data<ServerState>, id: Path<i32>, body: Json<NewBank>) -> HttpResponse {
    let bank = body.into_inner();
    let id = id.into_inner();

    let mut http_response_code = if state.service.bank(id).is_ok() {
        HttpResponse::Ok()
    } else {
        HttpResponse::Created()
    };

    let result = state.service.update_bank(Bank {
        id: Some(id),
        name: bank.name,
        routing_number: bank.routing_number,
        claims_account: bank.claims_account,
        adapter: bank.adapter.into(),
    });
    match result {
        Ok(val) => {
            let return_bank: ApiBank = val.into();
            http_response_code.json(return_bank)
        }
        Err(error) => create_banks_error_response(error),
    }
}

#[actix_web::delete("/{id}")]
fn delete_bank(state: Data<ServerState>, id: Path<i32>) -> HttpResponse {
    let result = state.service.delete_bank(id.into_inner());
    match result {
        Ok(_) => HttpResponse::Ok().json(""),
        Err(error) => create_banks_error_response(error),
    }
}
