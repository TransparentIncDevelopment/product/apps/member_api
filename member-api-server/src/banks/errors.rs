pub mod constants;

use crate::banks::errors::constants::{
    FRIENDLY_ACCESS_FAILURE_MESSAGE, FRIENDLY_NOT_FOUND_MESSAGE,
    FRIENDLY_REPOSITORY_OR_QUERY_FAILURE_MESSAGE, FRIENDLY_UNKNOWN_FAILURE_MESSAGE,
    FRIENDLY_VALIDATION_FAILURE_MESSAGE,
};
use actix_web::HttpResponse;
use member_api::banks::repository::errors::BanksRepositoryError;

use crate::{error::ErrorContent, LoggingEvent};

pub(crate) fn create_http_error_response(error: BanksRepositoryError) -> HttpResponse {
    let error_content = create_error_content(error.clone());
    match error {
        BanksRepositoryError::Access { .. } => {
            HttpResponse::ServiceUnavailable().json(error_content)
        }

        BanksRepositoryError::Validation { .. } => HttpResponse::BadRequest().json(error_content),

        BanksRepositoryError::Unknown { .. } => {
            HttpResponse::InternalServerError().json(error_content)
        }
        BanksRepositoryError::NotFound => HttpResponse::NotFound().json(error_content),
        BanksRepositoryError::Repository { .. } | BanksRepositoryError::QueryError { .. } => {
            HttpResponse::BadRequest().json(error_content)
        }
    }
}

fn log_error_and_create_friendly_error_content(
    msg: String,
    obfuscated_msg: String,
    should_obfuscate: bool,
) -> ErrorContent {
    error!(LoggingEvent::BanksRepositoryError(msg.clone()));
    ErrorContent {
        message: if should_obfuscate {
            obfuscated_msg
        } else {
            msg
        },
    }
}

fn create_error_content(error: BanksRepositoryError) -> ErrorContent {
    let generalize_error_message = error.clone().contains_internal_db_msg();
    match error {
        BanksRepositoryError::Access {
            msg,
            source: _source,
        } => log_error_and_create_friendly_error_content(
            msg,
            FRIENDLY_ACCESS_FAILURE_MESSAGE.to_string(),
            generalize_error_message,
        ),
        BanksRepositoryError::Validation {
            msg,
            source: _source,
        } => log_error_and_create_friendly_error_content(
            msg,
            FRIENDLY_VALIDATION_FAILURE_MESSAGE.to_string(),
            generalize_error_message,
        ),
        BanksRepositoryError::Unknown {
            msg,
            source: _source,
        } => log_error_and_create_friendly_error_content(
            msg,
            FRIENDLY_UNKNOWN_FAILURE_MESSAGE.to_string(),
            generalize_error_message,
        ),
        BanksRepositoryError::NotFound => log_error_and_create_friendly_error_content(
            FRIENDLY_NOT_FOUND_MESSAGE.to_string(),
            FRIENDLY_NOT_FOUND_MESSAGE.to_string(),
            generalize_error_message,
        ),
        BanksRepositoryError::Repository {
            msg,
            source: _source,
        }
        | BanksRepositoryError::QueryError {
            msg,
            source: _source,
        } => log_error_and_create_friendly_error_content(
            msg,
            FRIENDLY_REPOSITORY_OR_QUERY_FAILURE_MESSAGE.to_string(),
            generalize_error_message,
        ),
    }
}

#[cfg(test)]
pub mod tests {
    use crate::banks::errors::create_http_error_response;
    use http::StatusCode;
    use member_api::banks::repository::errors::{BanksRepositoryError, BanksRepositoryErrorSource};

    #[test]
    fn banks_api_maps_repository_access_failures() {
        let failure = BanksRepositoryError::Access {
            msg: "Arbitrary Failure!".to_string(),
            source: BanksRepositoryErrorSource::BanksRepository,
        };

        let failure = create_http_error_response(failure);

        assert_eq!(failure.status(), StatusCode::SERVICE_UNAVAILABLE)
    }

    #[test]
    fn banks_api_maps_repository_validation_failures() {
        let failure = BanksRepositoryError::Validation {
            msg: "Arbitrary Failure!".to_string(),
            source: BanksRepositoryErrorSource::BanksRepository,
        };

        let failure = create_http_error_response(failure);

        assert_eq!(failure.status(), StatusCode::BAD_REQUEST)
    }

    #[test]
    fn banks_api_maps_repository_unknown_failures() {
        let failure = BanksRepositoryError::Unknown {
            msg: "Arbitrary Failure!".to_string(),
            source: BanksRepositoryErrorSource::BanksRepository,
        };

        let failure = create_http_error_response(failure);

        assert_eq!(failure.status(), StatusCode::INTERNAL_SERVER_ERROR)
    }

    #[test]
    fn banks_api_maps_repository_not_found_failures() {
        let failure = BanksRepositoryError::NotFound;

        let failure = create_http_error_response(failure);

        assert_eq!(failure.status(), StatusCode::NOT_FOUND)
    }

    #[test]
    fn banks_api_maps_repository_operation_failure() {
        let failure = BanksRepositoryError::Repository {
            msg: "Arbitrary Failure!".to_string(),
            source: BanksRepositoryErrorSource::BanksRepository,
        };

        let failure = create_http_error_response(failure);

        assert_eq!(failure.status(), StatusCode::BAD_REQUEST)
    }
}
