use crate::web_server::ServerState;
use actix_web::{
    web::{scope, Data},
    HttpResponse, ResponseError, Scope,
};
use xand_address::Address;

pub fn routes() -> Scope {
    scope("/api/v1/member")
        .service(get_address)
        .service(get_xand_network_balance)
}

#[derive(Clone, Debug, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PagingSpec {
    pub page_size: Option<usize>,
    pub page_number: Option<usize>,
}

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AddressResponse {
    pub address: Address,
}

#[actix_web::get("/address")]
pub fn get_address(state: Data<ServerState>) -> HttpResponse {
    HttpResponse::Ok().json(AddressResponse {
        address: state.service.address().clone(),
    })
}

#[derive(Debug, Default, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct XandBalance {
    balance_in_minor_unit: u64,
}

#[actix_web::get("/balance")]
async fn get_xand_network_balance(state: Data<ServerState>) -> HttpResponse {
    let result: Result<u64, crate::Error> = state.service.xand_balance().await.map_err(Into::into);
    match result {
        Ok(value) => HttpResponse::Ok().json(XandBalance {
            balance_in_minor_unit: value,
        }),
        Err(e) => e.error_response(),
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        member::{AddressResponse, XandBalance},
        service::MockMemberService,
        tests::{
            test_context::{web_service::client::TestWebServiceClient, TestContext},
            ParseResponse,
        },
    };
    use http::StatusCode;
    use std::str::FromStr;
    use xand_address::Address;

    #[actix_rt::test]
    async fn route_address() {
        // Given
        let address = Address::from_str(&TestContext::default_address()).unwrap();

        let mut service = MockMemberService::new();
        service.expect_address().return_const(address.clone());

        let context = TestContext::default();
        let app = context.start_web_server_with_service(service).await;

        // When
        let resp = app.get("/api/v1/member/address").await;

        // Then
        assert_eq!(StatusCode::OK, resp.status());

        let result: AddressResponse = resp.parse().await.unwrap();
        assert_eq!(result.address, address);
    }

    #[actix_rt::test]
    async fn route_balance() {
        // Given
        const BALANCE: u64 = 123456789;
        let mut service = MockMemberService::new();
        service
            .expect_xand_balance()
            .returning(|| Box::pin(async { Ok(BALANCE) }));

        let context = TestContext::default();
        let app = context.start_web_server_with_service(service).await;

        // When
        let resp = app.get("/api/v1/member/balance").await;

        // Then
        assert_eq!(StatusCode::OK, resp.status());

        let result: XandBalance = resp.parse().await.unwrap();
        assert_eq!(result.balance_in_minor_unit, BALANCE);
    }
}
