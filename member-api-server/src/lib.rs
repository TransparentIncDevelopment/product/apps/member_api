#![forbid(unsafe_code)]
#![allow(non_snake_case)]
// To improve portability and avoid future surprises, we require all Futures implement Send. Actix's
// runtime allows for them to violate this, as requests are handled end-to-end on a single thread,
// but this is not true of all runtimes or users.
#![deny(clippy::future_not_send)]

extern crate config as cfg;
extern crate derive_new;
extern crate diesel;
extern crate diesel_migrations;
extern crate futures;
extern crate glob;
extern crate jsonwebtoken as jwt;

#[macro_use]
extern crate serde;
#[macro_use]
extern crate tpfs_logger_port;

pub use error::Error;
use member_api::config::ConfigWrapper;

use crate::cli::{handler::generate_jwt_token_cli, Cli, Command};
pub use crate::logging::*;
use panics::set_log_panics_hook;

mod accounts;
mod auth;
mod banks;
pub mod cli;
mod error;
mod health;
mod logging;
mod member;
mod service;
#[cfg(test)]
mod tests;
mod transaction;
mod web_server;

pub type Result<T, E = crate::error::Error> = std::result::Result<T, E>;

/// Member API's programmatically invokable entrypoint.
/// Note that this will initialize a global logger and interact with stdin.
/// If called programmatically, this will likely cause much conflict with existing loggers and access
/// to stdin/stdout if not managed carefully.
/// TODO: Refaactor this entrypoint to be more friendly if needing to be consumed outside main.rs
///     ADO Item: https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/5686
/// A note on "allow(clippy::future_not_send)":
///   This crate explicitly enables the future_not_send lint, but it is impossible to maintain that
///   through the entry point due to Actix's HttpServer not being Send. See top of file for context.
#[allow(clippy::future_not_send)]
pub async fn lib_main(options: Cli, enable_logging_to_stdout: bool) -> Result<()> {
    if enable_logging_to_stdout {
        set_log_panics_hook();
        tpfs_logger_log4rs_adapter::init_with_default_config()
            .expect("Must be able to init logger");
    }

    // Run alternative subcommand or default to starting web server
    match options.generate_jwt {
        Some(Command::Jwt(args)) => generate_jwt_token_cli(args),
        None => {
            let config =
                ConfigWrapper::load(vec!["/etc/member-api", "./config", "config", "../config"])
                    .expect("Failed to read Member API configuration");
            info!(LoggingEvent::Informational(format!("{:?}", config)));
            crate::web_server::start(config).await
        }
    }
}
