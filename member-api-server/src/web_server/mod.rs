pub mod home;

use actix_web::{web::JsonConfig, HttpResponse};

use crate::{
    accounts::routes as accounts, banks::routes as banks, health, logging::LoggingEvent, member,
    transaction, Result,
};
use actix_cors::Cors;
use actix_web::{web::ServiceConfig, App, HttpServer};

use crate::error::ErrorContent;
use member_api::config::ConfigWrapper;

use crate::{auth::RouteConfig, service::MemberService};
use member_api::MemberApiService;

pub struct ServerState {
    pub service: Box<dyn MemberService>,
}

pub fn create_route_config(secret: &Option<String>) -> Result<RouteConfig> {
    match secret {
        Some(secret) => {
            info!(LoggingEvent::Informational("JWT auth enabled".to_string()));

            Ok(RouteConfig::default_from_secret(secret))
        }
        None => Ok(RouteConfig::noauth()),
    }
}

// This crate explicitly enables the future_not_send lint, but it is impossible to maintain that
// through the entry point due to Actix's HttpServer not being Send. See lib.rs for context.
#[allow(clippy::future_not_send)]
pub async fn start(config: ConfigWrapper) -> Result<()> {
    let port = config.port().unwrap_or(3000);
    let cores = num_cpus::get();
    let default_worker_count = cores - 1;
    let mut workers = config.workers().unwrap_or(default_worker_count);
    if workers < 8 {
        // reasonable default number of workers.
        workers = 8;
    }
    let working_directory = std::env::current_dir().unwrap();

    info!(LoggingEvent::Informational(format!(
        "Started member-api in {:?}; Workers: {}; Port: {}; Cores: {}",
        working_directory, workers, port, cores
    )));

    let route_config = create_route_config(&config.jwt_secret()?)?;

    let service = Box::new(MemberApiService::from_config(&config)?);

    Ok(HttpServer::new(move || {
        // 32 threads created on startup.

        // Specifying CORS to be allowed for anyone in case partner's host this on a different
        // domain than a website that will make requests to the Member API.
        let cors = Cors::default()
            .allow_any_origin()
            .allow_any_method()
            .allow_any_header()
            .max_age(3600);

        App::new()
            .wrap(route_config.clone())
            .wrap(cors)
            .wrap(actix_web::middleware::Logger::default())
            .app_data(JsonConfig::default().error_handler(|err, _req| {
                ::log::error!("{}", err);
                let message = format!("Error deserializating request: {}", err);
                actix_web::error::InternalError::from_response(
                    err,
                    HttpResponse::BadRequest().json(ErrorContent { message }),
                )
                .into()
            }))
            .configure(|c| {
                let state = ServerState {
                    service: service.clone(),
                };

                configure(c, state);
            })
    })
    .workers(workers)
    .keep_alive(15) // If request takes longer than this to process, actix will kill it. In a Nginx+K8s context, this comes back to the user as a "502 Bad Gateway"
    .shutdown_timeout(60) // Allow a timeout grace-period so remaining work can flush.
    .client_timeout(15) // Set client timeout to 15 seconds to send headers
    .client_shutdown(15) // Set client shutdown timeout to 15 seconds before dropping request.
    .bind(format!("0.0.0.0:{}", port))
    .expect("Member API failed to start http server.")
    .run()
    .await?)
}

///
/// This method adds the `ServerState` and routes to the actix web
/// app. It exists as a way to dependency inject this part of the
/// configuration into test versions of the actix web application.
/// This is important because there is no visibility into routes once
/// configured which creates the possibility that the route configuration
/// we're testing against doesn't match the one we're using at runtime.
pub fn configure(input: &mut ServiceConfig, state: ServerState) {
    input
        .app_data(actix_web::web::Data::new(state))
        .service(accounts::routes())
        .service(banks::routes())
        .service(health::routes())
        .service(member::routes())
        .service(transaction::routes())
        .service(home::routes());
}
