use actix_web::{web::scope, HttpResponse, Scope};

include!(concat!(env!("OUT_DIR"), "/generated.rs"));

/// Configure routes on an http server.
pub fn routes() -> Scope {
    let generated = generate();

    scope("/")
        .service(actix_web_static_files::ResourceFiles::new(
            "/content", generated,
        ))
        .service(home)
}

#[actix_web::get("")]
fn home() -> HttpResponse {
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .body(include_str!("../../content/swagger/swagger.html"))
}
