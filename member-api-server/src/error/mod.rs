pub mod web;

use crate::logging::LoggingEvent;
use actix_web::{HttpResponse, ResponseError};
use thiserror::Error;
use xand_address::AddressError;

#[allow(clippy::large_enum_variant)]
#[derive(Clone, Debug, Serialize, Error)]
pub enum Error {
    #[error("{}", source)]
    Http {
        #[from]
        source: web::ActixError,
    },

    #[error("{}", message)]
    Serialization { message: String },

    #[error("{}", message)]
    Validation { message: String },

    #[error("{}", message)]
    Generic { message: String },

    #[error("Invalid address in request: {}", source)]
    InvalidAddress {
        #[from]
        source: AddressError,
    },

    #[error("{}", source)]
    MemberService {
        #[from]
        source: member_api::Error,
    },

    #[error("IO Error: {}", message)]
    IoError { message: String },
}

impl From<std::io::Error> for Error {
    fn from(io_err: std::io::Error) -> Self {
        Error::IoError {
            message: io_err.to_string(),
        }
    }
}

impl From<serde_json::error::Error> for Error {
    fn from(e: serde_json::error::Error) -> Self {
        Error::Serialization {
            message: e.to_string(),
        }
    }
}

impl From<std::string::FromUtf8Error> for Error {
    fn from(e: std::string::FromUtf8Error) -> Self {
        Error::Serialization {
            message: e.to_string(),
        }
    }
}

#[derive(Debug, Default, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct ErrorContent {
    pub message: String,
}

pub trait ResponseType {
    fn response_type(&self) -> HttpResponse;
}

impl ResponseError for crate::Error {
    fn error_response(&self) -> HttpResponse {
        error!(LoggingEvent::Error(self.clone()));

        match self {
            Self::MemberService { source } => self.create_http_error_response(source),
            Self::Validation { message } => HttpResponse::BadRequest().json(message),
            Self::InvalidAddress { source } => HttpResponse::BadRequest()
                .json(format!("The address requested was invalid: {}", source)),
            _ => HttpResponse::InternalServerError().json(self),
        }
    }
}

impl crate::Error {
    fn create_http_error_response(&self, source: &member_api::Error) -> HttpResponse {
        match source {
            member_api::Error::NotFound { message } => HttpResponse::NotFound().json(message),
            member_api::Error::Validation { message } => HttpResponse::BadRequest().json(message),
            member_api::Error::InvalidAddress { source } => HttpResponse::BadRequest()
                .json(format!("The address requested was invalid: {}", source)),
            member_api::Error::RequestTimeout => HttpResponse::RequestTimeout().finish(),
            member_api::Error::Unauthorized { message } => {
                HttpResponse::Unauthorized().json(message)
            }
            _ => HttpResponse::InternalServerError().json(self),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::{error::web::ActixError, Error};
    use http::StatusCode;
    use std::sync::Arc;
    use xand_address::AddressError;

    #[test]
    fn mailbox_error() {
        let source = Arc::new(actix::MailboxError::Closed);

        let error: Error = ActixError::MailBox { source }.into();

        let response = error.error_response();

        assert_eq!(StatusCode::INTERNAL_SERVER_ERROR, response.status());
    }

    #[test]
    fn url_generation() {
        let source: Error = ActixError::UrlGeneration {
            message: "Error Test".to_string(),
        }
        .into();

        let response = source.error_response();

        assert_eq!(StatusCode::INTERNAL_SERVER_ERROR, response.status());
    }

    #[test]
    fn serialization_failed() {
        let source = Error::Serialization {
            message: "Error Text".into(),
        };

        let response = source.error_response();

        assert_eq!(StatusCode::INTERNAL_SERVER_ERROR, response.status());
    }

    #[test]
    fn generic() {
        let source = Error::Generic {
            message: "Test Message".to_string(),
        };

        let response = source.error_response();

        assert_eq!(StatusCode::INTERNAL_SERVER_ERROR, response.status());
    }

    #[test]
    fn notfound() {
        let source: Error = Error::MemberService {
            source: member_api::Error::NotFound {
                message: "Account not found".to_string(),
            },
        };

        let response = source.error_response();

        assert_eq!(StatusCode::NOT_FOUND, response.status());
    }

    #[test]
    fn timeout() {
        let source: Error = Error::MemberService {
            source: member_api::Error::RequestTimeout,
        };

        let response = source.error_response();

        assert_eq!(StatusCode::REQUEST_TIMEOUT, response.status());
    }

    #[test]
    fn validation() {
        let source: Error = Error::Validation {
            message: "Error Text".to_string(),
        };

        let response = source.error_response();

        assert_eq!(StatusCode::BAD_REQUEST, response.status());
    }

    #[test]
    fn unauthorized() {
        let source: Error = Error::MemberService {
            source: member_api::Error::Unauthorized {
                message: "Member API Can't authenticate to the XAND API".to_string(),
            },
        };

        let response = source.error_response();

        assert_eq!(StatusCode::UNAUTHORIZED, response.status());
    }

    #[test]
    fn invalid_address() {
        let source: Error = Error::InvalidAddress {
            source: AddressError::InvalidSs58Checksum,
        };

        let response = source.error_response();

        assert_eq!(StatusCode::BAD_REQUEST, response.status());
    }
}
