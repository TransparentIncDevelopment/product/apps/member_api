use crate::{auth::RouteConfig, error::ErrorContent, web_server::ServerState};
use actix_service::ServiceFactory;
use actix_web::{
    body::Body,
    dev::{ServiceRequest, ServiceResponse},
    web::JsonConfig,
    App, Error,
};

pub use self::client::TestWebServiceClient;

pub(crate) mod client;

pub async fn start_web_service_test_instance(
    server_state: ServerState,
    route_config: RouteConfig,
) -> impl TestWebServiceClient {
    let app = build_web_app(server_state, route_config);
    actix_web::test::init_service(app).await
}

fn build_web_app(
    server_state: ServerState,
    route_config: RouteConfig,
) -> App<
    impl ServiceFactory<
        ServiceRequest,
        Config = (),
        Response = ServiceResponse<Body>,
        Error = Error,
        InitError = (),
    >,
    Body,
> {
    // I hate not being able to have a common way to set middleware for both the
    // runtime and test-time versions of the actix-web app, but
    // I can't figure out how to derive a common interface for the two
    // `app` variables.
    //
    App::new()
        //
        // Additional sad face for not being able to get logging
        // to work in this. It apparently changes the signature of
        // the `wrap()` function which breaks everything in ways
        // that are beyond me to address.
        //
        // .wrap(actix_web::middleware::Logger::default())
        .wrap(route_config)
        .app_data(JsonConfig::default().error_handler(|err, _req| {
            ::log::error!("{}", err);
            let message = format!("Error deserializating request: {}", err);
            actix_web::error::InternalError::from_response(
                err,
                actix_web::HttpResponse::BadRequest().json(ErrorContent { message }),
            )
            .into()
        }))
        .configure(|c| {
            crate::web_server::configure(c, server_state);
        })
}
