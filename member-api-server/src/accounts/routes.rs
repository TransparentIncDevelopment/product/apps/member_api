use crate::accounts::{
    errors::create_http_error_response, AccountBalanceResponse, EditAccount, NewAccount,
};
use actix_web::{
    web::{scope, Data, Json, Path},
    HttpResponse, Scope,
};
use member_api::{
    accounts::{web::ApiAccount, Account},
    Error,
};

use crate::{error::ErrorContent, web_server::ServerState};

///
/// Configure routes on an http server.
pub fn routes() -> Scope {
    scope("/api/v1/accounts")
        .service(get_accounts)
        .service(get_account)
        .service(get_balance)
        .service(post_account)
        .service(put_account)
        .service(delete_account)
}

#[actix_web::get("")]
fn get_accounts(state: Data<ServerState>) -> HttpResponse {
    let result = state.service.accounts();
    match result {
        Ok(val) => {
            let results: Vec<ApiAccount> = val.into_iter().map(|e| e.into()).collect();
            HttpResponse::Ok().json(&results)
        }
        Err(error) => create_accounts_error_response(error),
    }
}

fn create_accounts_error_response(error: member_api::Error) -> HttpResponse {
    match error {
        Error::AccountRepositoryErrors { source } => create_http_error_response(source),
        e => HttpResponse::InternalServerError().json(ErrorContent {
            message: e.to_string(),
        }),
    }
}

#[actix_web::get("/{id}")]
fn get_account(state: Data<ServerState>, id: Path<i32>) -> HttpResponse {
    let result = state.service.account(id.into_inner());
    match result {
        Ok(val) => {
            let result: ApiAccount = val.into();
            HttpResponse::Ok().json(result)
        }
        Err(error) => create_accounts_error_response(error),
    }
}

#[actix_web::post("")]
fn post_account(state: Data<ServerState>, body: Json<NewAccount>) -> HttpResponse {
    let new_account = body.into_inner();
    let account: Account = new_account.into();

    let result = state.service.add_account(account);

    match result {
        Ok(val) => {
            let result: ApiAccount = val.into();
            HttpResponse::Created().json(result)
        }
        Err(error) => create_accounts_error_response(error),
    }
}

#[actix_web::put("/{id}")]
fn put_account(state: Data<ServerState>, id: Path<i32>, body: Json<EditAccount>) -> HttpResponse {
    let account = body.into_inner();
    let id = id.into_inner();

    let mut http_response_code = if state.service.account(id).is_ok() {
        HttpResponse::Ok()
    } else {
        HttpResponse::Created()
    };

    let result = state.service.update_account(Account {
        id: Some(id),
        bank_id: 0,
        short_name: account.short_name,
        account_number: account.account_number,
    });
    match result {
        Ok(val) => {
            let return_account: ApiAccount = val.into();
            http_response_code.json(return_account)
        }
        Err(error) => create_accounts_error_response(error),
    }
}

#[actix_web::delete("/{id}")]
fn delete_account(state: Data<ServerState>, id: Path<i32>) -> HttpResponse {
    let result = state.service.delete_account(id.into_inner());
    match result {
        Ok(_) => HttpResponse::Ok().json(""),
        Err(error) => create_accounts_error_response(error),
    }
}

#[actix_web::get("/{id}/balance")]
async fn get_balance(state: Data<ServerState>, id: Path<i32>) -> HttpResponse {
    let account_id = id.to_string();
    let result = state.service.bank_balance(id.into_inner()).await;
    match result {
        Ok(val) => HttpResponse::Ok().json(AccountBalanceResponse {
            account_id,
            available_balance_in_minor_unit: val.available_balance_in_minor_unit,
            current_balance_in_minor_unit: val.current_balance_in_minor_unit,
        }),
        Err(error) => create_accounts_error_response(error),
    }
}
