# actix-web Error Handling

This document explains how to manage converting result types from `snafu` errors to those used by `actix-web` when making web requests, within the `member-api` codebase.

This approach deals with:
* Utilization of `snafu` macros to get context out of `actix_web` errors
* Defining custom response types with `From` impls
* Converting serializable response types with the custom `debug_serialize()` function 
* Logging with `LoggingEvents` in the response handlers

The current `actix_web` documentation can be found [here](https://docs.rs/actix-web/1.0.7/actix_web/).

The current `snafu` documentation can be found [here](https://docs.rs/snafu/0.4.3/snafu/).

### Tl;dr

* Redefining the response type happens in `error/actix_web.rs`' `error_response()` method
* Because Actix always returns a Future, which we have to handle the response error conversion with a `snafu` domain-specific error
* Quick Example: `bank/actix_web.rs` `fiat_accounts()` which return `actix_web` errors which we have to manage in our `error` crate
* Logging happens in `error/actix_web.rs`' `render_response()` method, which determines the shape of the log data - **NOTE:** this location may move into a centralized response handler in the future

## Background 

`actix-web` is a Rust web framework used for request routing in our `member-api`. In order to process authentication 
requests, it requires serialization and deserialization of the web requests. 
Read more about [serialization and deserialization](https://stackoverflow.com/questions/1360632/what-are-object-serialization-and-deserialization).

At the time of writing, we use the `serde` crate for serialization and deserialization. `serde` provides a `derive` 
macro to generate implementations of the `Serialize` and `Deserialize` traits for data structures defined in a crate, 
allowing them to be represented conveniently in all of Serde's data formats. 
Read more about the [derive macro](https://serde.rs/derive.html).

We are also using `serde` enum variant attributes to serialize error types using a function that is different from 
`serde`'s implementation of `Serialize`. To do so, we much also define the function implementation, because it differs from the default. Read more about the `serde` enum
[variant attributes](https://serde.rs/variant-attrs.html)

### Relevant Dependencies

* [serde](https://crates.io/crates/serde) 
// A generic serialization/deserialization framework.
* [serde_derive](https://crates.io/crates/serde_derive) 
// Macros 1.1 implementation of #[derive(Serialize, 
Deserialize)].
* [serde_json](https://crates.io/crates/serde_json) 
// A JSON serialization file format.

## Setup

Our project is setup according to the modularized domain error architecture described [here](../docs/ERRORS.md). 
```
Rust Project

.
├── Cargo.toml
├── src
    ├── main.rs
    ├── bank
        ├── actix_web.rs
    ├── error
        ├── mod.rs
        ├──actix_web.rs
```

### Handling Errors

We want to be able to handle `actix_web` errors in our route handlers. 

Let's start by looking at the `fiat_accounts()` handler in `bank/actix_web.rs`:

```rust
/// Configure routes on an http server.
pub fn routes() -> Scope {
    scope("/bank").service(
        scope("/accounts")
            .route("", get().to_async(fiat_accounts))
            .route("/{accountId}", get().to_async(fiat_account))
            .route("/{accountId}/balance", get().to_async(fiat_balance))
            .route(
                "/{accountId}/fundClaims",
                post().to_async(fiat_transfer),
            ),
    )
}
```

The `fiat_accounts()` function returns a Future with `actix_web::Error` as its error type:

```rust
fn fiat_accounts(
    state: Data<ServerState>,
) -> impl Future<Item = HttpResponse, Error = actix_web::Error> {
    state
        .bank_dispatcher
        .send(GetAccountsMsg)
        .from_err()
        .and_then(result)
        .map_err(Into::into)
        .and_then(|result| {
            HttpResponse::Ok()
                .content_type("application/json")
                .body(serde_json::to_string(&result).unwrap())
        })
}
```

### Defining Error Result Types

OK, but what specific error do we want to be returning? 

We will define what error to return for `actix_web::Error` in `error/mod.rs`:

```rust
impl From<actix_web::ActixError> for Error {
    fn from(source: actix_web::ActixError) -> Self {
        Error::Http { source }
    }
}
```

`impl From` allows us to implement getting whatever we have defined in our `ActixError` error handling enum and returning it as an `Error` in our route handler caller code. So in this case, we are returning some kind of `actix_web::ActixError`. 

To summarize, we got some context from the `fiat_accounts()` function's error return, and have implemented that `Error` context to return a custom error format using our `ActixError` enum definitions. 

#### Managing Response Types in Rust

Now, in our error domain handling crate `error/actix_web.rs`, we setup some logic so that we don't have to specify which specific error will be returned. This also allows us to  standardize the handling of our Result response types for the `actix_web` external dependency. 

We first define an error enum `ActixError`:

```rust
#[derive(Clone, Debug, Serialize, Snafu)]
#[snafu(visibility(pub))] // Required here or else must be declared in using module
pub enum ActixError {
    #[snafu(display("Actix mailbox: {}", source))]
    MailBox {
        #[snafu(source(from(actix::MailboxError, Arc::new)))]
        #[serde(serialize_with = "xand_base::snafu_extensions::debug_serialize")]
        source: Arc<actix::MailboxError>,
    },
    #[snafu(display("Generating URL: {}", message))]
    UrlGeneration {
        // Couldn't fit actix_web::error::UrlGenerationError into the source
        message: String,
    },
}
```

In this `ActixError` enum, we define the error source definitions for specific `actix_web` errors such as `MailboxError`, as well as any other errors our code generates. 

This error enum uses the derive macro and proc macros as described in the [ERRORS.md](../docs/ERRORS.md) documentation. Specifically, we use `derive(Snafu)` to generate a struct in the background for each of the enumeration variants we have defined:

We then impl the shape of each error variant context:

```rust
impl From<actix::MailboxError> for Error {
    fn from(e: actix::MailboxError) -> Self {
        ActixError::MailBox {
            source: Arc::new(e),
        }
        .into()
    }
}

impl From<actix_web::error::UrlGenerationError> for Error {
    fn from(error: actix_web::error::UrlGenerationError) -> Error {
        ActixError::UrlGeneration {
            message: format!("{}", error),
        }
        .into()
    }
}
```

We handle the impls differently according to the error's specification in the `ActixError` enum.

>Note: The `into()` ensures that Rust coerces the specific error (e.g.`UrlGenerationError`) into `Error`. 

* **`UrlGeneration` error**: we only need to handle string formatting because it only returns a message typed String

* **`MailBox` error**: returns an error context source, and so we need to handle it in a special way that impls two things we need for logging: `Clone` and `Serialize`. 

#### Impl Clone and Serializable

When we added the `MailBox` and `UrlGeneration` error variants to the `ActixError` enum in `error/actix_web.rs`, we also needed to ensure that our errors impl `Clone` and `Serialize`.

Example:

```rust
#[derive(Clone, Debug, Serialize, Snafu)]
#[snafu(visibility(pub))] // Required here or else must be declared in using module
pub enum ActixError {
    #[snafu(display("Actix mailbox: {}", source))]
    MailBox {
        #[snafu(source(from(actix::MailboxError, Arc::new)))]
        #[serde(serialize_with = "xand_base::snafu_extensions::debug_serialize")]
        source: Arc<actix::MailboxError>,
    },
    #[snafu(display("Generating URL: {}", message))]
    UrlGeneration {
        // Couldn't fit actix_web::error::UrlGenerationError into the source
        message: String,
    },
}
```

We have a slight problem, because one of our errors, `actix::MailBoxError` doesn't impl`Clone` _or_ `Serialize` , so we need to resolve both of those issues.

**First**, to make `actix::MailBoxError` error impl `Clone`, we needed to wrap it in an atomic reference counted (`Arc`) [pointer](https://doc.rust-lang.org/std/sync/struct.Arc.html) `source: Arc<actix::MailboxError>`. This defines the Error source type as an `Arc`. 

We also needed to add the tag `#[snafu(source(from(actix::MailboxError, Arc::new)))]` to allow the caller to ignore the underlying error and automatically wrap the source error when you call .context(). 

**Second**, our error source doesn't impl `Serialize`, so we specialized a serialization method with `serde` by creation a function that can be passed into the Serde `serialize_with` called `debug_serialize`. This function relies on the debug implementation to generate a serialization. 

To do so, we added a proc macro `#[serde(serialize_with = "xand_base::snafu_extensions::debug_serialize")]` above the source definition, to allow us to get the source from the error.

We also define the custom serialization function in the same crate:

```rust
use serde::Serializer;
...

fn debug_serialize<S>(x: &dyn Debug, s: S) -> std::result::Result<S::Ok, S::Error>
where
    S: Serializer,
{
    s.serialize_str(&format!("{:?}", x))
}
```

This function defines how we implement `Serialize` for the error context. 

### Response Handling

Because a multitude of crates use `actix_web`, we have to define how their errors will be returned in `error/actix_web.rs`.

We will handle our responses by impl `ResponseError` for any `Error`:

```rust
impl ResponseError for Error {
    fn error_response(&self) -> HttpResponse {
        match self {
            Self::Auth { source } => source.response_type(),
            Self::Bank { source } => source.response_type(),
            Self::TxnHistory { source } => source.response_type(),
            Self::NotFound { .. } => HttpResponse::NotFound().finish(),
            _ => HttpResponse::InternalServerError().finish(),
        }
    }

    fn render_response(&self) -> HttpResponse {
        error!(LoggingEvent::Error(self.clone())); 
        let mut resp = self.error_response();
        let body = APIError {
            error: ErrorContent {
                code: 0,
                message: format!("{}", self),
                originated_from: "".to_string(),
            },
        };
        resp.headers_mut().insert(
            header::CONTENT_TYPE,
            header::HeaderValue::from_static("application/json"),
        );
        resp.set_body(Body::from(serde_json::to_string(&body).unwrap()))
    }
}
```

The above function takes the context of an `Error`, and according to the domain of the error, returns an error context response appropriate to how we can handle that error. 

## Logging Errors

We define logging all `actix-web` errors in `error/actix_web.rs`:
```rust

    fn render_response(&self) -> HttpResponse {
        error!(LoggingEvent::Error(self.clone()));
```
The above function defines the shape of the Error using the LoggingEvent enum found in `logging/mod.rs`.
