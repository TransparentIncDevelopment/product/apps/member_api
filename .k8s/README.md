# Understanding this k8s configuration

These kubernetes files are designed for consumption by Octopus Deploy. While we no longer deploy to Octopus, this is still an effective way to configure Kubernetes for GCP Deployment. Values in the `yaml` that will change between environments are templated with Octopus variables (e.g., #{kubernetes-namespace}).

## Required Tools

* jq
* zip
* kubectl 1.17 or greater
* kustomize

```bash
sudo apt install jq zip

sudo apt-get update && sudo apt-get install -y apt-transport-https
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl

```

Go [here](https://kubernetes-sigs.github.io/kustomize/installation/binaries/
) for instructions to install `kustomize`. 

## Configuring Kubernetes for GCP Development

> For development, we deploy to a k8s cluster running in Google Kubernetes Engine. You will need to authenticate with Google Cloud beforehand.

Step 1: Connect to the `dev` cluster.

```
make connect-to-dev-cluster
```

Step 2: Deploy member api to your cluster.

```
make build deploy
```

Step 3: Confirm your deployment

```
make health
```

If you are having problems, you can check the pod with 

```
make inspect
```

If the pod didn't start, you can check the deployment logs with 

```
make deployment-logs 
```

## Basic Workflow

```mermaid
sequenceDiagram
    engineer-->>CLI: Execute make build
    CLI-->>local: Produce k8s files ready for deployment
    engineer-->>CLI: Execute make deploy
    CLI-->>k8s: Execute the deployment
```

```bash
make build
bat .output/$(USER).yaml # you are using bat instead of cat right?
make deploy
```

## Make Targets

These make targets are available in the `.k8s` directory.

| Command | Description |
|--|--|
| print | echoes the variables that will be used for other targets to `stdout`. |
| clean | Removes the `.output` directory and contents |
| kustomize | Runs `kubectl kustomize` on the k8s yamls, producing `.output/$(USER).yaml`. |
| configure | Mimics Octopus' variable substution on the $(OUTPUT_FILE). This is really just `sed` replacements. |
| build | Runs the `print`, `kustomize`, and `configure` targets to produce a valid k8s document ready for deployment to the `$USER`s namespace. |
| deploy | runs `kubectl apply` on the $(OUTPUT_FILE) |
| package | Produces a package ready to be published. If run locally, the package will be specific to $USER. If run in `gitlab`, the package will be `beta` on any non-master branch, and `release` on master. |
| publish-artifactory | Publishes the package to Artifactory. |

One or more targets can be strung together. 

```bash
make clean build deploy

rm -rf .output
CONFIGURATION
Image Version      : '1.4.5'
Image              : 'gcr.io/xand-dev/xand-bank-mocks:1.4.5'
Environment        : 'crmckenzie'
Output File        : '.output/crmckenzie.yaml'
Package Version    : '1.4.5-crmckenzie'
Package            : '.output/xand-bank-mocks-k8s.1.4.5-crmckenzie.zip'
CI_COMMIT_REF_NAME : ''
CI_PIPELINE_ID     : ''

mkdir -p .output
kubectl kustomize base > .output/crmckenzie.yaml
sed -i 's/#{member.address}/member-api/g' .output/crmckenzie.yaml
sed -i 's/#{member-api-url}/member-api.#{kubernetes.namespace}.dev.xand.tools/g' .output/crmckenzie.yaml
sed -i 's/#{member-api-tls-secret}/member-api-#{kubernetes.namespace}-dev-xand-tools/g' .output/crmckenzie.yaml
sed -i 's/#{docker.image.name}/gcr.io\/xand-dev\/xand-bank-mocks:1.4.5/g' .output/crmckenzie.yaml
sed -i 's/#{kubernetes.namespace}/crmckenzie/g' .output/crmckenzie.yaml
kubectl apply -f .output/crmckenzie.yaml
namespace/crmckenzie configured
configmap/xand-bank-mocks-config-map-89628fkmh5 configured
service/xand-bank-mocks created
deployment.apps/xand-bank-mocks created
ingress.extensions/xand-bank-mocks-ingress created
```

## Octopus Variables

| Variable Name | Meaning |
| -- | -- |
| kubernetes.namespace | The kubernetes namespace into which the resources will be deployed. |
| member.address | The member address. |
| docker.image.name | The fully qualified name of the docker image. |
| member-api-url | The url used for the "hosts" property in the Ingress. |
| member-api-tls-secret | The name used to store the tls secret for member api. |

