# Gitlab CI Docker Image for Member API

This is the docker image used to build and publish member api. In order to be used, it needs to be built and published, and its version should be bumped on each change.

To build the image run `make build`. To publish the image run `make publish.` You can update the dependency version in the `./gitlab-ci.yaml` file after the image is published.