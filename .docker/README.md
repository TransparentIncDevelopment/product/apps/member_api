# Docker
The `Makefile` in this directory helps manage the member api docker container.

## Dependencies

This makefile relies on `toml-cli` to extract the version from `cargo.toml`.

```bash
cargo install toml-cli
```

## Typical workflows

1. In the root dir run `cargo build --release`
1. cd into `.docker`
1. `make artifacts build` produces a container using the local bits. The container name will be something like `1.0.0-crmckenzie` where "crmckenzie" is replaced with your username.

## Image Publishing

Do not publish your local image unless you are specifically testing something related to docker image publishing. In general let
our CI pipeline take care of publishing. The Makefile is designed to enable semantic versioning during the CI pipeline.

## Commands

### print

`make print` outputs the variables that will be used for other commands in the makefile.

### artifacts

`make artifacts` assumes that member api has been compiled for the release target (`cargo build --release`).

It copies the member api binary into `.docker/tmp` so that it is accessible by the docker file.

This copy dance is necessary because otherwise docker will ship the entire member_api directory to the docker
daemon _before_ picking out the small subset of files we need to load into the container. This wouldn't
be an issue except the `/target` folder typically contains multiple-gigabytes of files. The member api by contrast
is about 26Mb. 

### build

`make build` assumes `make artifacts` has been previously called. It just does the work to create the container
from local bits.

### check-version

`make check-version` will verify that the container does _not_ exist in the registry. This target
will fail if the container already exists.

### publish

`make publish` will publish the container to the registry. It runs `check-version` to protect against
overwriting containers.

### run

`make run` will run the locally produced container.

### kill

`make kill` will kill the locally produced container.

### shell

`make shell` will allow you to log into the running container and execute bash commands.