use thiserror::Error;

use crate::{
    accounts::{
        repository::{errors::AccountsRepositoryError, AccountsRepository},
        AccountDetails,
    },
    banks::{
        api::{
            bank_adapter_factory::BankAdapterFactory, errors::XandBanksClientConstructionError,
            BankApi,
        },
        repository::{errors::BanksRepositoryError, BanksRepository},
        Bank,
    },
    health::*,
};

pub struct BankHealthService {
    account: AccountDetails,
    bank: Bank,
    bank_api: BankApi,
}

#[derive(Debug, Error, Clone, Serialize)]
pub enum BanksServiceCreationError {
    #[error("failed to fetch accounts")]
    AccountRetrieval(#[from] AccountsRepositoryError),
    #[error("failed to fetch bank details")]
    BankRetrieval(#[from] BanksRepositoryError),
    #[error("failed to create bank adapter")]
    BankAdapter(#[from] XandBanksClientConstructionError),
}

#[async_trait::async_trait]
impl Service for BankHealthService {
    fn name(&self) -> String {
        self.bank.name.clone()
    }

    async fn health(&self) -> ServiceStatus {
        match self
            .bank_api
            .balance(self.account.account_number.as_str())
            .await
        {
            Ok(_) => ServiceStatus::Up,
            Err(e) => ServiceStatus::Down {
                details: format!("{:?}", e),
            },
        }
    }
}

impl BankHealthService {
    /// Since banks can't be counted on to provide health endpoints,
    /// This method iterates over the banks and choose the first
    /// account for each configured bank. It will then get the balance
    /// of that account as a proxy for a health check.
    pub fn create_services(
        banks: &dyn BanksRepository,
        accounts: &dyn AccountsRepository,
        bank_adapters: &dyn BankAdapterFactory,
    ) -> Result<Vec<Box<dyn Service>>, BanksServiceCreationError> {
        let selected_accounts = accounts
            .get_single_per_bank()
            .map_err(BanksServiceCreationError::AccountRetrieval)?;

        let mut services = Vec::new();
        for account in selected_accounts {
            let bank = banks
                .get(account.bank_id)
                .map_err(BanksServiceCreationError::BankRetrieval)?;
            let result: Box<dyn Service> = Box::new(BankHealthService {
                account: account.clone(),
                bank: bank.clone(),
                bank_api: bank_adapters
                    .create(bank.adapter)
                    .map_err(BanksServiceCreationError::BankAdapter)?,
            });

            services.push(result);
        }

        Ok(services)
    }
}

#[cfg(test)]
pub mod bank_health_tests {
    use assert_matches::assert_matches;
    use xand_banks::{errors::XandBanksErrors, models::BankBalance, xand_money::Usd};

    use crate::{
        accounts::Account,
        banks::{Bank, BankAdapter},
        tests::{TestContext, TestDataBuilder},
    };

    use super::*;

    #[test]
    fn one_per_bank() {
        // Given
        let context = TestContext::default();
        let bank1 = Bank {
            name: "Fredbob".to_string(),
            id: None,
            routing_number: "999".to_string(),
            claims_account: "doesn't matter".to_string(),
            adapter: BankAdapter::build(1),
        };
        let bank2 = Bank {
            name: "Foobar".to_string(),
            id: None,
            routing_number: "888".to_string(),
            claims_account: "doesn't matter".to_string(),
            adapter: BankAdapter::build(2),
        };

        let bank1 = context.banks_repository.insert(bank1).unwrap();
        let bank2 = context.banks_repository.insert(bank2).unwrap();

        context
            .accounts_repository
            .insert(Account {
                short_name: "doesn't matter".to_string(),
                account_number: "123".to_string(),
                id: None,
                bank_id: bank1.id.unwrap(),
            })
            .unwrap();
        context
            .accounts_repository
            .insert(Account {
                short_name: "doesn't matter".to_string(),
                account_number: "456".to_string(),
                id: None,
                bank_id: bank2.id.unwrap(),
            })
            .unwrap();

        // When
        let results = BankHealthService::create_services(
            &context.banks_repository,
            &context.accounts_repository,
            &context.bank_api_factory,
        )
        .unwrap();

        // Then
        assert_eq!(results.len(), 2);
        assert_eq!(results[0].name(), "Fredbob".to_string());
        assert_eq!(results[1].name(), "Foobar".to_string());
    }

    #[test]
    fn banks_with_no_account_omitted() {
        // Given
        let context = TestContext::default();
        let bank1 = Bank {
            name: "Fredbob".to_string(),
            id: None,
            routing_number: "999".to_string(),
            claims_account: "doesn't matter".to_string(),
            adapter: BankAdapter::build(1),
        };
        let bank2 = Bank {
            name: "Foobar".to_string(),
            id: None,
            routing_number: "888".to_string(),
            claims_account: "doesn't matter".to_string(),
            adapter: BankAdapter::build(2),
        };

        let bank1 = context.banks_repository.insert(bank1).unwrap();
        let _bank2 = context.banks_repository.insert(bank2).unwrap();

        context
            .accounts_repository
            .insert(Account {
                short_name: "doesn't matter".to_string(),
                account_number: "123".to_string(),
                id: None,
                bank_id: bank1.id.unwrap(),
            })
            .unwrap();
        // no account for bank2

        // When
        let results = BankHealthService::create_services(
            &context.banks_repository,
            &context.accounts_repository,
            &context.bank_api_factory,
        )
        .unwrap();

        // Then
        assert_eq!(results.len(), 1);
        assert_eq!(results[0].name(), "Fredbob".to_string());
    }

    #[tokio::test]
    async fn healthy_with_valid_environment() {
        // Given
        let context = TestContext::default();
        let bank = Bank {
            name: "Fredbob".to_string(),
            id: None,
            routing_number: "999".to_string(),
            claims_account: "doesn't matter".to_string(),
            adapter: BankAdapter::build(1),
        };

        context
            .xand_banks_api
            .mock_get_balance
            .return_ok(BankBalance {
                available_balance: Usd::default(),
                current_balance: Usd::default(),
            });

        context.bank_api_factory.mock_create.return_ok(BankApi {
            adapter: Box::new(context.xand_banks_api.clone()),
        });

        let bank = context.banks_repository.insert(bank).unwrap();

        context
            .accounts_repository
            .insert(Account {
                short_name: "doesn't matter".to_string(),
                account_number: "123".to_string(),
                bank_id: bank.id.unwrap(),
                id: None,
            })
            .unwrap();

        // When
        let services = BankHealthService::create_services(
            &context.banks_repository,
            &context.accounts_repository,
            &context.bank_api_factory,
        )
        .unwrap();

        assert_eq!(services.len(), 1);
        let service = &services[0];
        let health = service.health().await;

        // Then
        assert_eq!(health, ServiceStatus::Up);
    }

    #[tokio::test]
    async fn unhealthy_when_balance_fails() {
        // Given
        let context = TestContext::default();
        let bank = Bank {
            name: "Fredbob".to_string(),
            id: None,
            routing_number: "999".to_string(),
            claims_account: "doesn't matter".to_string(),
            adapter: BankAdapter::build(1),
        };

        context
            .xand_banks_api
            .mock_get_balance
            .return_err(XandBanksErrors::General {
                message: "offline".to_string(),
            });
        context.bank_api_factory.mock_create.return_ok(BankApi {
            adapter: Box::new(context.xand_banks_api.clone()),
        });

        let bank = context.banks_repository.insert(bank).unwrap();

        context
            .accounts_repository
            .insert(Account {
                short_name: "doesn't matter".to_string(),
                account_number: "123".to_string(),
                bank_id: bank.id.unwrap(),
                id: None,
            })
            .unwrap();

        // When
        let services = BankHealthService::create_services(
            &context.banks_repository,
            &context.accounts_repository,
            &context.bank_api_factory,
        )
        .unwrap();

        assert_eq!(services.len(), 1);
        let service = &services[0];
        let health = service.health().await;

        // Then
        assert_matches!(health, ServiceStatus::Down { .. });
    }
}
