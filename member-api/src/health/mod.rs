#[macro_use]
pub mod banks;
pub mod accounts_repository;
pub mod banks_repository;
pub mod invalid;
pub mod secret_store;
pub mod xand_api;

/// This entire file can be extracted into a crate
/// and reused in other applications if we so desire.

/// Apply this trait to component dependencies such as
/// API clients, database clients, etc. Use it to report
/// the overall health of the system.
///
/// Intended use-case is to back a /health endpoint.
#[async_trait::async_trait]
pub trait Service: Send + Sync {
    fn name(&self) -> String;
    async fn health(&self) -> ServiceStatus;
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum ServiceStatus {
    Up,
    Down { details: String },
}

/// Public contract intended for API serialization.
///
/// ## Example Output
///
/// ```json
/// curl localhost:3000/health
/// {
///    "down" : [],
///    "up" : [
///       {
///          "status" : "up",
///          "service" : "XAND API"
///       }
///    ]
/// }
///
/// curl localhost:3000/health
/// {
///     "down" : [
///        {
///           "service" : "XAND API",
///           "status" : "down",
///           "details" : "Cannot connect to XAND API."
///        }
///     ],
///     "up" : []
/// }
/// ```
#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ServiceHealth {
    pub up: Vec<HealthCheckResult>,
    pub down: Vec<HealthCheckResult>,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct HealthCheckResult {
    service: String,
    status: HealthCheckStatus,
    #[serde(skip_serializing_if = "Option::is_none")]
    details: Option<String>,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub enum HealthCheckStatus {
    Up,
    Down,
}

impl ServiceHealth {
    /// Returns a ServiceHealth result for a list of services.
    pub async fn check(services: Vec<Box<dyn Service>>) -> ServiceHealth {
        let mut checks: Vec<HealthCheckResult> = vec![];
        for service in services {
            let mut check: HealthCheckResult = service.health().await.into();
            check.service = service.name();
            checks.push(check);
        }

        let (up, down) = checks.into_iter().partition(|c| c.up());

        ServiceHealth { up, down }
    }

    /// true if all services are up. Otherwise false.
    pub fn up(&self) -> bool {
        self.down.is_empty()
    }
}

impl HealthCheckResult {
    fn up(&self) -> bool {
        self.status == HealthCheckStatus::Up
    }
}

impl From<ServiceStatus> for HealthCheckResult {
    fn from(input: ServiceStatus) -> Self {
        let mut contract = HealthCheckResult {
            service: "".to_string(),
            status: HealthCheckStatus::Up,
            details: None,
        };

        if let ServiceStatus::Down { details } = input {
            contract.details = Some(details);
            contract.status = HealthCheckStatus::Down;
        }

        contract
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    struct AlwaysUpService {}

    #[async_trait::async_trait]
    impl Service for AlwaysUpService {
        fn name(&self) -> String {
            "Always Up".to_string()
        }

        async fn health(&self) -> ServiceStatus {
            ServiceStatus::Up
        }
    }

    struct AlwaysDownService {}

    #[async_trait::async_trait]
    impl Service for AlwaysDownService {
        fn name(&self) -> String {
            "Always Down".to_string()
        }

        async fn health(&self) -> ServiceStatus {
            ServiceStatus::Down {
                details: "Big Surprise".to_string(),
            }
        }
    }

    #[tokio::test]
    async fn health_check_up() {
        let service: Box<dyn Service> = Box::new(AlwaysUpService {});

        let result = service.health().await;

        assert_eq!(result, ServiceStatus::Up);
    }

    #[tokio::test]
    async fn health_check_down() {
        let service: Box<dyn Service> = Box::new(AlwaysDownService {});

        let result = service.health().await;

        assert_eq!(
            result,
            ServiceStatus::Down {
                details: "Big Surprise".to_string()
            }
        );
    }

    #[tokio::test]
    async fn service_health_check() {
        let services: Vec<Box<dyn Service>> =
            vec![Box::new(AlwaysUpService {}), Box::new(AlwaysDownService {})];

        let result = ServiceHealth::check(services).await;

        assert_eq!(
            result,
            ServiceHealth {
                up: vec![HealthCheckResult {
                    service: "Always Up".to_string(),
                    status: HealthCheckStatus::Up,
                    details: None,
                },],
                down: vec![HealthCheckResult {
                    service: "Always Down".to_string(),
                    status: HealthCheckStatus::Down,
                    details: Some("Big Surprise".to_string())
                }]
            }
        );
    }

    #[tokio::test]
    async fn service_health_up_when_all_services_are_running() {
        let services: Vec<Box<dyn Service>> =
            vec![Box::new(AlwaysUpService {}), Box::new(AlwaysUpService {})];

        let health = ServiceHealth::check(services).await;

        assert!(health.up());
    }

    #[tokio::test]
    async fn service_health_up_when_not_all_services_are_running() {
        let services: Vec<Box<dyn Service>> =
            vec![Box::new(AlwaysUpService {}), Box::new(AlwaysDownService {})];

        let health = ServiceHealth::check(services).await;

        assert!(!health.up());
    }
}
