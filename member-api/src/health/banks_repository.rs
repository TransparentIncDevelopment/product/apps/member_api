use crate::{
    banks::repository::{errors::BanksRepositoryError, BanksRepository},
    LoggingEvent,
};

use super::{Service, ServiceStatus};

pub struct BanksRepositoryHealthService {
    repository: Box<dyn BanksRepository>,
}

impl BanksRepositoryHealthService {
    fn log_check_health_error(error: BanksRepositoryError) {
        error!(LoggingEvent::ErrorMessageOnly(format!(
            "Banks repository health-check returned error. {:?}",
            error
        )));
    }
}

#[async_trait::async_trait]
impl Service for BanksRepositoryHealthService {
    fn name(&self) -> String {
        String::from("Banks storage backend")
    }

    async fn health(&self) -> ServiceStatus {
        self.repository
            .check_health()
            .err()
            .map_or(ServiceStatus::Up, |e| {
                Self::log_check_health_error(e);
                ServiceStatus::Down {
                    details: String::from("An error occurred while accessing the store."),
                }
            })
    }
}

impl BanksRepositoryHealthService {
    pub fn new(repository: Box<dyn BanksRepository>) -> Self {
        Self { repository }
    }
}

#[cfg(test)]
pub mod tests {
    use crate::{
        banks::{
            repository::{errors::BanksRepositoryError, sqlite::SqliteBanksRepository},
            testing::MockBanksRepository,
        },
        health::{Service, ServiceHealth},
        tests::test_context::temp_db::TestDatabase,
    };

    use super::BanksRepositoryHealthService;
    use crate::banks::repository::errors::BanksRepositoryErrorSource;

    #[tokio::test]
    async fn service_health_up_with_healthy_repository() {
        let banks_repository = MockBanksRepository::default();
        banks_repository.mock_check_health.return_ok(());

        let services: Vec<Box<dyn Service>> = vec![Box::new(BanksRepositoryHealthService::new(
            Box::new(banks_repository),
        ))];

        let health = ServiceHealth::check(services).await;

        assert!(health.up());
    }

    #[tokio::test]
    async fn service_health_down_with_erroring_repository() {
        let banks_repository = MockBanksRepository::default();
        banks_repository
            .mock_check_health
            .return_err(BanksRepositoryError::Access {
                msg: "An arbitrary error occurred".into(),
                source: BanksRepositoryErrorSource::BanksRepository,
            });
        let services: Vec<Box<dyn Service>> = vec![Box::new(BanksRepositoryHealthService::new(
            Box::new(banks_repository),
        ))];

        let health = ServiceHealth::check(services).await;

        assert!(!health.up());
    }

    #[tokio::test]
    async fn integ_service_health_down_with_erroring_sqlite_repository() {
        // Given
        let db = TestDatabase::default();
        // intentionally omitted db initialization; no migrations have been applied.
        let banks_repository = SqliteBanksRepository::new(db.conn_pool());

        // When
        let services: Vec<Box<dyn Service>> = vec![Box::new(BanksRepositoryHealthService::new(
            Box::new(banks_repository),
        ))];
        let health = ServiceHealth::check(services).await;

        // Then
        assert!(!health.up());
        assert_eq!(health.down.len(), 1);
        assert_eq!(health.down[0].service.as_str(), "Banks storage backend");
    }
}
