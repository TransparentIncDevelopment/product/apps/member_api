use crate::{blockchain::Blockchain, health::*, LoggingEvent};

pub struct XandApiClientHealthChecker {
    pub xand_api: Box<dyn Blockchain>,
}

#[async_trait::async_trait]
impl Service for XandApiClientHealthChecker {
    fn name(&self) -> String {
        "Xand API".to_string()
    }

    async fn health(&self) -> ServiceStatus {
        match self.xand_api.check_connectivity().await {
            Ok(_) => ServiceStatus::Up,
            Err(e) => {
                error!(LoggingEvent::ErrorWithMessage(
                    "Xand API health check error".into(),
                    e
                ));
                ServiceStatus::Down {
                    details: "Cannot connect to XAND API.".to_string(),
                }
            }
        }
    }
}
