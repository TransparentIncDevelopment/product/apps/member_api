table! {
    account (id) {
        id -> Integer,
        short_name -> Text,
        account_number -> Text,
        bank_id -> Integer,
    }
}

table! {
    adapter_type (id) {
        id -> Integer,
        #[sql_name = "type"]
        type_ -> Text,
    }
}

table! {
    bank (id) {
        id -> Integer,
        name -> Text,
        routing_number -> Text,
        claims_account -> Text,
        adapter_type_id -> Integer,
        url -> Text,
        mcb_secret_user_name -> Nullable<Text>,
        mcb_secret_password -> Nullable<Text>,
        mcb_secret_client_app_ident -> Nullable<Text>,
        mcb_secret_organization_id -> Nullable<Text>,
        tp_secret_api_key_id -> Nullable<Text>,
        tp_secret_api_key_value -> Nullable<Text>,
    }
}

joinable!(account -> bank (bank_id));
joinable!(bank -> adapter_type (adapter_type_id));

allow_tables_to_appear_in_same_query!(account, adapter_type, bank,);
