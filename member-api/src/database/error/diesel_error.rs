use super::DatabaseError;
use diesel::result::{DatabaseErrorKind, Error};

impl From<Error> for DatabaseError {
    fn from(error: Error) -> Self {
        ::log::error!("diesel error: {}", error);
        match error {
            Error::NotFound => DatabaseError::NotFound,
            Error::DatabaseError(kind, ref info) => match kind {
                DatabaseErrorKind::UniqueViolation => DatabaseError::ValidationError {
                    msg: format!("{:?} {:?}", kind, info),
                    is_internal_msg: true,
                },
                DatabaseErrorKind::ForeignKeyViolation => DatabaseError::ValidationError {
                    msg: format!("{:?} {:?}", kind, info),
                    is_internal_msg: true,
                },
                _ => DatabaseError::UnhandledError { source: error },
            },
            _ => DatabaseError::UnhandledError { source: error },
        }
    }
}

/// Extension trait for inspecting Diesel errors
pub(crate) trait DieselErrorExt {
    fn is_foreign_key_violation(&self) -> bool;
}

impl DieselErrorExt for Error {
    fn is_foreign_key_violation(&self) -> bool {
        matches!(
            self,
            Error::DatabaseError(DatabaseErrorKind::ForeignKeyViolation, _)
        )
    }
}
