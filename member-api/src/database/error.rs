use thiserror::Error;

pub(crate) mod diesel_error;

#[derive(Debug, Error)]
pub enum DatabaseError {
    #[error("{:?}", msg)]
    PathError { msg: String, is_internal_msg: bool },
    #[error("{:?}", source)]
    ConnectionError {
        #[from]
        source: DieselConnError,
    },
    #[error("{:?}", source)]
    MigrationError {
        #[from]
        source: diesel_migrations::RunMigrationsError,
    },
    #[error("{:?}", source)]
    UnhandledError { source: diesel::result::Error },
    #[error("{source}")]
    QueryError { source: diesel::result::Error },
    #[error("NotFound")]
    NotFound,
    #[error("{:?}", msg)]
    ValidationError { msg: String, is_internal_msg: bool },
    #[error("{:?}", source)]
    ParseError {
        #[from]
        source: url::ParseError,
    },
}

/// Error enum for capturing the different Error issues that diesel can return
#[derive(Debug, Error)]
pub enum DieselConnError {
    #[error("{:?}", source)]
    Pool {
        #[from]
        source: diesel::r2d2::Error,
    },
    #[error("{:?}", msg)]
    PoolBuild { msg: String },
    #[error("{:?}", source)]
    SingleConnection {
        #[from]
        source: diesel::ConnectionError,
    },
}
