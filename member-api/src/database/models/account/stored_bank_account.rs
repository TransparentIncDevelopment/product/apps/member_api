// https://stackoverflow.com/questions/56853059/use-of-undeclared-type-or-module-when-using-diesels-belongs-to-attribute
#[cfg(test)]
use crate::accounts::Account;
use crate::database::{models::bank::stored_bank::StoredBank, schema::account};

#[derive(
    Clone,
    Debug,
    Deserialize,
    Eq,
    PartialEq,
    Serialize,
    Default,
    Queryable,
    Identifiable,
    Associations,
)]
#[serde(rename_all = "kebab-case")]
#[belongs_to(StoredBank, foreign_key = "bank_id")]
#[table_name = "account"]
pub struct StoredBankAccount {
    pub id: i32,
    pub short_name: String,
    pub account_number: String,
    pub bank_id: i32,
}

#[cfg(test)]
impl StoredBankAccount {
    pub fn from_account(account: Account, bank_id: i32) -> StoredBankAccount {
        StoredBankAccount {
            id: account.id.unwrap_or_default(),
            short_name: account.short_name,
            account_number: account.account_number,
            bank_id,
        }
    }
}
