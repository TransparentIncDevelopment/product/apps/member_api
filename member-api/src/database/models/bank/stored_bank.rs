use crate::{
    banks::{BankAdapter, McbAdapterConfig, TreasuryPrimeAdapterConfig},
    database::{models::adapter_type::StoredAdapterType, schema::bank, DatabaseError},
};

#[derive(
    Clone,
    Debug,
    Deserialize,
    Eq,
    PartialEq,
    Serialize,
    Default,
    Queryable,
    Identifiable,
    Associations,
)]
#[serde(rename_all = "kebab-case")]
#[table_name = "bank"]
#[belongs_to(StoredAdapterType, foreign_key = "adapter_type_id")]
pub struct StoredBank {
    pub id: i32,
    pub name: String,
    pub routing_number: String,
    pub claims_account: String,
    pub adapter_type_id: i32,
    pub url: String,
    pub mcb_secret_user_name: Option<String>,
    pub mcb_secret_password: Option<String>,
    pub mcb_secret_client_app_ident: Option<String>,
    pub mcb_secret_organization_id: Option<String>,
    pub tp_secret_api_key_id: Option<String>,
    pub tp_secret_api_key_value: Option<String>,
}

const MCB_ADAPTER_ID: i32 = 1;
const TREASURE_PRIME_ADAPTER_ID: i32 = 2;

impl StoredBank {
    pub fn get_bank_adapter_id(adapter: &BankAdapter) -> i32 {
        match adapter {
            BankAdapter::Mcb(_) => Self::mcb_id(),
            BankAdapter::TreasuryPrime(_) => Self::treasury_prime_id(),
        }
    }

    pub fn mcb_id() -> i32 {
        MCB_ADAPTER_ID
    }

    pub fn treasury_prime_id() -> i32 {
        TREASURE_PRIME_ADAPTER_ID
    }

    pub fn map_adapter(&self) -> Result<BankAdapter, DatabaseError> {
        match self.adapter_type_id {
            MCB_ADAPTER_ID => {
                let url = self.url.parse()?;

                Ok(BankAdapter::Mcb(McbAdapterConfig {
                    url,
                    secret_client_app_ident: self
                        .mcb_secret_client_app_ident
                        .clone()
                        .unwrap_or_default(),
                    secret_organization_id: self
                        .mcb_secret_organization_id
                        .clone()
                        .unwrap_or_default(),
                    secret_password: self.mcb_secret_password.clone().unwrap_or_default(),
                    secret_user_name: self.mcb_secret_user_name.clone().unwrap_or_default(),
                }))
            }
            TREASURE_PRIME_ADAPTER_ID => {
                let url = self.url.parse()?;

                Ok(BankAdapter::TreasuryPrime(TreasuryPrimeAdapterConfig {
                    url,
                    secret_api_key_id: self.tp_secret_api_key_id.clone().unwrap_or_default(),
                    secret_api_key_value: self.tp_secret_api_key_value.clone().unwrap_or_default(),
                }))
            }
            _ => Err(DatabaseError::ValidationError {
                msg: format!(
                    "Could not create bank adapter keys from adapter type {}: bank_id: {}",
                    self.adapter_type_id, self.id
                ),
                is_internal_msg: false,
            }),
        }
    }
}
