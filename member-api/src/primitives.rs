use chrono::Utc;

pub type DateTime = chrono::prelude::DateTime<Utc>;
pub type Result<T, E = crate::error::Error> = std::result::Result<T, E>;

#[derive(Clone, Debug, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Paged<T> {
    pub page: usize,
    pub size: usize,
    pub items: Vec<T>,
}
