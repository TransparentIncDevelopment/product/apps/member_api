use super::*;
use crate::error::Error;
use std::{fs::File, io::Write, path::Path, str::FromStr};
use tempfile::{tempdir, TempDir};
use xand_address::Address;
use xand_secrets::ExposeSecret;
use xand_secrets_local_file::LocalFileSecretStoreConfiguration;
use xand_secrets_vault::VaultConfiguration;

fn string_to_file(dir: &TempDir, config_string: &str, filename: &str) -> String {
    let file_path = dir.path().join(filename);
    let mut file = File::create(file_path.clone()).unwrap();
    writeln!(file, "{}", config_string).unwrap();
    file_path.to_str().unwrap().to_string()
}

fn config_from_file_string(
    config_str: &str,
    filename: &str,
) -> std::result::Result<MemberApiConfig, ConfigurationError> {
    let dir = tempdir().unwrap();
    string_to_file(&dir, config_str, filename);
    let cfg_wrapper = ConfigWrapper::load(vec![dir.path().to_str().unwrap()])?;
    Ok(cfg_wrapper.0)
}

const XAND_ONLY_CONFIG: &str = r#"
            address = "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"
            [xand-api]
            url = "https://some.path.com:8008/api/v1"
            [secret-store]
            vault = { http-endpoint = "", token = "" }
        "#;
#[test]
fn xand_api_url_is_configured() {
    let cfg = config_from_file_string(XAND_ONLY_CONFIG, "config.toml").unwrap();

    assert_eq!(
        cfg.xand_api.url.to_string(),
        "https://some.path.com:8008/api/v1"
    );
}

#[test]
fn when_database_is_not_configured_then_default_database_is_configured() {
    let cfg = config_from_file_string(XAND_ONLY_CONFIG, "config.toml").unwrap();

    assert_eq!(cfg.database, Path::new("./data/member-api.db"));
}

#[test]
fn when_database_is_configured_then_it_is_parsed() {
    let config_str = r#"
database: "/some/path/file.db"
address: "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"
xand-api:
    url: "http://127.0.0.1:10044"
secret-store:
    vault:
        http-endpoint: ""
        token: ""
"#;

    let cfg = config_from_file_string(config_str, "config.yaml").unwrap();

    assert_eq!(cfg.database, Path::new("/some/path/file.db"));
}

#[test]
fn address_is_configured() {
    let cfg = config_from_file_string(XAND_ONLY_CONFIG, "config.toml").unwrap();
    let expected: Address =
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap();

    assert_eq!(cfg.address, expected);
}

#[test]
fn load_invalid_address_fails() {
    // Given
    let valid_address =
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap();
    let address_missing_char = &valid_address.to_string()[1..];

    let invalid_address_cfg = format!(
        r#"
            address = {}
            [xand-api]
            url = "https://some.path.com:8008/api/v1"
        "#,
        address_missing_char
    );
    let cfg_dir = tempdir().unwrap();
    string_to_file(&cfg_dir, &invalid_address_cfg, "config.toml");

    // When
    let cfg = ConfigWrapper::load(vec![cfg_dir.path().to_str().unwrap()]);

    // Then
    assert!(cfg.is_err());
}

#[test]
fn address_is_required() {
    // Given
    let no_address_cfg: &str = r#"
            [xand-api]
            url = "https://some.path.com:8008/api/v1"
        "#;
    let cfg_dir = tempdir().unwrap();
    string_to_file(&cfg_dir, no_address_cfg, "config.toml");

    // When
    let cfg = ConfigWrapper::load(vec![cfg_dir.path().to_str().unwrap()]);

    // Then
    assert!(cfg.is_err());
}

#[test]
fn when_xand_api_auth_is_not_configured_then_none_is_parsed() {
    let cfg = config_from_file_string(XAND_ONLY_CONFIG, "config.toml").unwrap();

    assert!(
        cfg.xand_api.auth.is_none(),
        "Expected None, got {:?}",
        cfg.xand_api.auth
    );
}

#[test]
fn when_xand_api_auth_jwt_is_configured_then_it_is_parsed() {
    let config_str = r#"
address: "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"
xand-api:
    url: "http://127.0.0.1:10044"
    auth:
        jwt: "abc123"
secret-store:
    vault:
        http-endpoint: ""
        token: ""
"#;

    let cfg = config_from_file_string(config_str, "config.yaml").unwrap();

    assert_eq!(cfg.xand_api.auth.unwrap().jwt.unwrap(), "abc123");
}

#[test]
fn reads_xand_api_jwt_secret() {
    // given a secrets file
    let secrets_str = r#"
        most-secret-auth-ever: "SEEEEEECRETTTTTTT!!!!"
    "#;

    let cfg_dir = tempdir().unwrap();

    let secrets_file = string_to_file(&cfg_dir, secrets_str, "secrets.yaml");

    // and a config pointing to that secrets file
    let config_str = format!(
        r#"
        address: "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"
        xand-api:
            url: "http://127.0.0.1:10044"
            auth:
                jwt: "most-secret-auth-ever"
        secret-store:
            local-file:
                yaml-file-path: {}
    "#,
        secrets_file
    );

    string_to_file(&cfg_dir, &config_str, "config.yaml");

    let cfg = ConfigWrapper::load(vec![cfg_dir.path().to_str().unwrap()]).unwrap();

    let expected_secret_key = cfg.0.xand_api.auth.clone().unwrap().jwt.unwrap();

    assert_eq!(
        cfg.get_secret_config_value(&expected_secret_key)
            .unwrap()
            .expose_secret(),
        "SEEEEEECRETTTTTTT!!!!"
    )
}

#[test]
fn reads_member_api_secret() {
    // given a secrets file
    let secrets_str = r#"
        most-secret-auth-ever: "SEEEEEECRETTTTTTT!!!!"
    "#;

    let cfg_dir = tempdir().unwrap();

    let secrets_file = string_to_file(&cfg_dir, secrets_str, "secrets.yaml");

    // and a config pointing to that secrets file
    let config_str = format!(
        r#"
        address: "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"
        jwt-secret: "most-secret-auth-ever"
        xand-api:
            url: "http://127.0.0.1:10044"
        secret-store:
            local-file:
                yaml-file-path: {}
    "#,
        secrets_file
    );

    string_to_file(&cfg_dir, &config_str, "config.yaml");

    // When we load the config
    let cfg = ConfigWrapper::load(vec![cfg_dir.path().to_str().unwrap()]).unwrap();

    // Then the secret it gives us corresponds to the correct entry in the secrets.yaml file
    assert_eq!(cfg.jwt_secret().unwrap().unwrap(), "SEEEEEECRETTTTTTT!!!!")
}

#[test]
fn secret_key_with_no_value_returns_error() {
    // given a secrets file
    let secrets_str = r#"
        most-secret-auth-ever: "SEEEEEECRETTTTTTT!!!!"
    "#;

    let cfg_dir = tempdir().unwrap();

    let secrets_file = string_to_file(&cfg_dir, secrets_str, "secrets.yaml");

    // and a config pointing to that secrets file
    // with a nonexistent secret key
    let config_str = format!(
        r#"
        address: "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"
        jwt-secret: "typo-auth-secret"
        xand-api:
            url: "http://127.0.0.1:10044"
        secret-store:
            local-file:
                yaml-file-path: {}
    "#,
        secrets_file
    );

    string_to_file(&cfg_dir, &config_str, "config.yaml");

    // When we load the config
    let cfg = ConfigWrapper::load(vec![cfg_dir.path().to_str().unwrap()]).unwrap();

    // Then it returns an error since there is no secret corresponding to the provided key
    assert!(matches!(cfg.jwt_secret(), Err(Error::SecretStore { .. })))
}

#[test]
fn secret_lookup_returns_none_if_none_provided() {
    let cfg_dir = tempdir().unwrap();

    // Given a config file with no jwt secret specified
    let config_str = r#"
        address: "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"
        xand-api:
            url: "http://127.0.0.1:10044"
        secret-store:
            local-file:
                yaml-file-path: path/to/file.yaml
    "#;

    string_to_file(&cfg_dir, config_str, "config.yaml");

    // When we load the config
    let cfg = ConfigWrapper::load(vec![cfg_dir.path().to_str().unwrap()]).unwrap();

    // Then it returns no error and no JWT Secret
    assert!(cfg.jwt_secret().unwrap().is_none())
}

#[test]
fn xand_api_timeout_can_be_set_via_config() {
    let config_str = r#"
address: "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"
xand-api:
    url: "http://127.0.0.1:10044"
    timeout-seconds: 15
secret-store:
    vault:
        http-endpoint: ""
        token: ""
"#;

    let cfg = config_from_file_string(config_str, "config.yaml").unwrap();
    assert_eq!(cfg.xand_api.timeout_seconds, Some(15));
}

#[test]
fn xand_api_timeout_is_optional() {
    let config_str = r#"
address: "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"
xand-api:
    url: "http://127.0.0.1:10044"
    auth:
        jwt: "abc123"
    # timeout-seconds: 15
secret-store:
    vault:
        http-endpoint: ""
        token: ""
"#;

    let cfg = config_from_file_string(config_str, "config.yaml").unwrap();

    assert_eq!(cfg.xand_api.timeout_seconds, None);
}

#[test]
fn when_member_api_jwt_secret_is_configured_then_it_is_parsed() {
    let config_str = r#"
address: "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"
xand-api:
    url: "http://127.0.0.1:10044"
jwt-secret: "secret!!"
secret-store:
    vault:
        http-endpoint: ""
        token: ""
"#;

    let cfg = config_from_file_string(config_str, "config.yaml").unwrap();

    assert_eq!(cfg.jwt_secret.unwrap(), "secret!!");
}

#[test]
fn config_dir_must_exist() {
    // When
    let paths = vec!["CONFIG_DIR_DOES_NOT_EXIST"];

    let result = ConfigWrapper::load(paths.clone());

    // named with underscore cuz clippy is dumb.
    let _paths_as_str: Vec<String> = paths.iter().map(|&s| s.to_string()).collect();

    // Then
    assert!(matches!(
        result,
        Err(ConfigurationError::NoConfigFoldersFound {
            paths: _paths_as_str,
        })
    ));
}

#[test]
fn config_dir_cannot_be_a_file() {
    // When

    let dir = tempdir().unwrap();

    let config_file = string_to_file(&dir, OK_CONFIG_STR, "config.yaml");

    let result = ConfigWrapper::load(vec![&config_file]);

    // Then
    assert!(matches!(
        result,
        Err(ConfigurationError::ConfigPathMustBeADirectory { path }) if path == config_file
    ));
}

#[test]
fn config_dirs_must_exist() {
    // When
    let paths = vec!["CONFIG_DIR_DOES_NOT_EXIST", "NEITHER_DOES_THIS_ONE"];

    let result = ConfigWrapper::load(paths.clone());

    // named with underscore cuz clippy is dumb.
    let _paths_as_str: Vec<String> = paths.iter().map(|&s| s.to_string()).collect();

    // Then
    assert!(matches!(
        result,
        Err(ConfigurationError::NoConfigFoldersFound {
            paths: _paths_as_str,
        })
    ));
}

#[test]
fn config_valid_when_dir_and_files_exist() -> std::result::Result<(), Box<dyn std::error::Error>> {
    // Given
    let dir = tempdir().unwrap();
    let config_path = dir.path().to_str().unwrap().to_string();
    let full_path: String = std::fs::canonicalize(config_path.clone())
        .unwrap()
        .to_str()
        .unwrap()
        .to_string();

    string_to_file(&dir, OK_CONFIG_STR, "config.yaml");

    let paths = vec![
        "CONFIG_DIR_DOES_NOT_EXIST",
        &config_path,
        "NEITHER_DOES_THIS_ONE",
    ];

    // When

    // Then

    let result = ConfigWrapper::load(paths)?;
    let actual_path: String = result.0.path.unwrap();
    assert_eq!(actual_path, full_path);

    Ok(())
}

#[test]
fn config_files_must_exist() {
    let dir = tempdir().unwrap();
    let config_path = dir.path().to_str().unwrap().to_string();
    let result = ConfigWrapper::load(vec![&config_path]);
    let expected_path = std::fs::canonicalize(config_path)
        .unwrap()
        .to_str()
        .unwrap()
        .to_owned();

    assert!(result.is_err(), "{:?}", result);

    match result {
        Err(ConfigurationError::NoConfigFilesFound { path }) => {
            assert_eq!(path, expected_path);
        }
        _ => {
            panic!("Invalid response: {:?}", result);
        }
    }
}

const OK_CONFIG_STR: &str = r#"
address: "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"
xand-api:
    url: "http://127.0.0.1:10044"
port: 3000
banks:
  - name: "mcb"
    routing-number: "026013356"
    claims-account: "5555555555"
    accounts:
      - id: "awesomeco"
        short-name: "Awesomeco's Checking Account"
        account-number: "3333333333"
      - id: "coolinc"
        short-name: "Coolinc's Checking Account"
        account-number: "4444444444"
    adapter:
      mcb:
        url: "http://localhost:8888/metropolitan/"
        auth:
          secret-path: "fake/secrets/mcb/path/"
          secret-key-username: "api_key_id"
          secret-key-password: "api_key_value"
        account-info-manager:
          secret-path: "secrets/dev/mcb"
          secret-key-client-app-ident: "client_app_ident_key"
          secret-key-organization-id: "organization_id_key"
secret-store:
    vault:
        http-endpoint: "http://test.vault.http.endpoint:2000"
        token: "vault_test_token123"

"#;

#[test]
fn xand_api_url_is_required() {
    let config_str = r#"
            chain_explorer_endpoint = "http://localhost:3001/api/"
            access_token_folder = "./"
            port = 0
        "#;

    let result = config_from_file_string(config_str, "config.toml");

    match result {
        Err(ConfigurationError::Format { message }) => {
            assert_eq!(message, "missing field `xand-api`".to_string());
        }
        _ => {
            panic!("Invalid response: {:?}", result);
        }
    }
}

#[test]
fn port_must_be_numeric() {
    let config_str = r#"
            chain_explorer_endpoint = "http://localhost:3001/api/"
            access_token_folder = "./"
            port = "uh oh not a number!!!!"
        "#;

    let result = config_from_file_string(config_str, "config.toml");
    assert!(result.is_err());

    if let Err(ConfigurationError::Format { message }) = result {
        assert!(message.starts_with("invalid type: string \"uh oh not a number!!!!\""));
    } else {
        panic!("expected ConfigurationError::Format. received {:?}", result);
    }
}

#[test]
fn missing_secret_store_configuration() {
    let config_str = r#"
        xand-api:
            url: "http://127.0.0.1:10044"
    "#;

    let result = config_from_file_string(config_str, "config.yaml");
    assert!(result.is_err());

    if let Err(ConfigurationError::Format { message }) = result {
        assert_eq!("missing field `secret-store`", message);
    } else {
        panic!("expected ConfigurationError::Format. received {:?}", result);
    }
}

#[test]
fn validate_vault_secret_store_configuration_omitted_certs() {
    let config_str = r#"
        address: "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"
        xand-api:
            url: "http://127.0.0.1:10044"
        secret-store:
            vault:
                http-endpoint: "http://test.vault.http.endpoint:2000"
                token: "vault_test_token123"
    "#;

    let result = config_from_file_string(config_str, "config.yaml");

    let config = result.unwrap();
    if let SecretStoreConfig::Vault(VaultConfiguration {
        http_endpoint,
        token,
        additional_https_root_certificate_files,
    }) = config.secret_store
    {
        assert_eq!(
            "http://test.vault.http.endpoint:2000",
            http_endpoint.as_str()
        );
        assert_eq!("vault_test_token123", token.expose_secret());
        assert!(additional_https_root_certificate_files.is_none());
    } else {
        panic!(
            "Expected valid VaultConfiguration, got {:?}",
            config.secret_store
        );
    };
}

#[test]
fn validate_local_file_secret_store_configuration() {
    let config_str = r#"
        address: "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"
        xand-api:
            url: "http://127.0.0.1:10044"
        secret-store:
            local-file:
                yaml-file-path: path/to/file.yaml
    "#;

    let result = config_from_file_string(config_str, "config.yaml");

    let config = result.unwrap();
    if let SecretStoreConfig::LocalFile(LocalFileSecretStoreConfiguration { yaml_file_path }) =
        config.secret_store
    {
        assert_eq!("path/to/file.yaml", yaml_file_path.as_str());
    } else {
        panic!(
            "Expected valid LocalFileSecretStoreConfiguration, got {:?}",
            config.secret_store
        );
    };
}

#[test]
fn secret_store_certs_parsed() {
    let config_str = r#"
        address: "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"
        xand-api:
            url: "http://127.0.0.1:10044"
        secret-store:
            vault:
                http-endpoint: "http://test.vault.http.endpoint:2000"
                token: "vault_test_token123"
                additional-https-root-certificate-files: [ "some/file.pem", "some/other_file.pem" ]
    "#;

    let result = config_from_file_string(config_str, "config.yaml");

    let config = result.unwrap();
    if let SecretStoreConfig::Vault(VaultConfiguration {
        http_endpoint,
        token,
        additional_https_root_certificate_files,
    }) = config.secret_store
    {
        assert_eq!(
            "http://test.vault.http.endpoint:2000",
            http_endpoint.as_str()
        );
        assert_eq!("vault_test_token123", token.expose_secret());
        assert!(additional_https_root_certificate_files.is_some());
        assert_eq!(
            Some(vec![
                String::from("some/file.pem"),
                String::from("some/other_file.pem"),
            ]),
            additional_https_root_certificate_files
        );
    } else {
        panic!(
            "Expected valid VaultConfiguration, got {:?}",
            config.secret_store
        );
    };
}
