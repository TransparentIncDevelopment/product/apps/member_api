use std::{io::Write, path::Path};
use tempfile::NamedTempFile;

pub struct TestSecrets {
    file: NamedTempFile,
}

impl TestSecrets {
    pub const SECRET_KEY: &'static str = "some-secret-key";
    pub const SECRET_VALUE: &'static str = "some-secret-value";

    pub fn file_path(&self) -> &Path {
        self.file.path()
    }
}

impl Default for TestSecrets {
    fn default() -> Self {
        let mut file = NamedTempFile::new().unwrap();
        let secrets_str = format!(
            "{}: \"{}\"",
            TestSecrets::SECRET_KEY,
            TestSecrets::SECRET_VALUE
        );
        writeln!(file, "{}", secrets_str).unwrap();
        Self { file }
    }
}
