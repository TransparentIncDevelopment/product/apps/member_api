use xand_address::Address;
use xand_api_client::{BankAccountId, BankAccountInfo};

use crate::{accounts::AccountDetails, transaction::Reference, Result};

pub mod xand_api_adapter;

/// An abstraction representing an implementation-agnostic blockchain.
#[async_trait::async_trait]
pub trait Blockchain: dyn_clone::DynClone + Send + Sync {
    async fn issue_create_request(&self, request: CreateRequest) -> Result<Receipt>;
    async fn redeem(&self, request: RedeemRequest) -> Result<Receipt>;
    async fn send_claims(&self, request: SendClaimsRequest) -> Result<Receipt>;
    async fn balance(&self, key: Address) -> Result<u64>;
    async fn check_connectivity(&self) -> Result<()>;
}
dyn_clone::clone_trait_object!(Blockchain);

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
pub struct CreateRequest {
    pub address: Address,
    pub account: BankAccountInfo,
    pub amount: u64,
    pub reference: Reference,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
pub struct RedeemRequest {
    pub address: Address,
    pub account: BankAccountInfo,
    pub amount: u64,
    pub reference: Reference,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize)]
pub struct SendClaimsRequest {
    pub address: Address,
    pub to: Address,
    pub amount: u64,
    pub reference: Reference,
}

#[derive(Clone, Debug, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Receipt {
    pub transaction_id: String,
    pub correlation_id: Option<String>,
}

impl std::fmt::Display for Receipt {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Receipt: {}", self.transaction_id)
    }
}

impl From<AccountDetails> for BankAccountInfo {
    fn from(input: AccountDetails) -> Self {
        BankAccountInfo::Unencrypted(BankAccountId {
            routing_number: input.routing_number,
            account_number: input.account_number,
        })
    }
}

#[cfg(test)]
pub mod test {
    use pseudo::Mock;
    use uuid::Uuid;
    use xand_api_client::TransactionId;

    use crate::Error;

    use super::*;

    impl Default for Receipt {
        fn default() -> Receipt {
            Receipt {
                transaction_id: TransactionId::default().to_string(),
                correlation_id: Some(Uuid::new_v4().to_string()),
            }
        }
    }

    #[derive(Clone)]
    pub struct MockBlockchain {
        pub mock_balance: Mock<Address, Result<u64>>,
        pub mock_create_request: Mock<CreateRequest, Result<Receipt>>,
        pub mock_redeem_request: Mock<RedeemRequest, Result<Receipt>>,
        pub mock_fund_claims_request: Mock<SendClaimsRequest, Result<Receipt>>,
        pub mock_check_connectivity: Mock<(), Result<()>>,
    }

    impl MockBlockchain {
        pub fn new() -> MockBlockchain {
            let error = Error::Generic {
                message: "MockBlockchain: Test Error".to_string(),
            };
            let mock_balance = Mock::new(Err(error.clone()));
            let mock_create_request = Mock::new(Err(error.clone()));
            let mock_redeem_request = Mock::new(Err(error.clone()));
            let mock_fund_claims_request = Mock::new(Err(error.clone()));
            let mock_check_connectivity = Mock::new(Err(error));

            MockBlockchain {
                mock_balance,
                mock_create_request,
                mock_redeem_request,
                mock_fund_claims_request,
                mock_check_connectivity,
            }
        }
    }

    impl Default for MockBlockchain {
        fn default() -> MockBlockchain {
            MockBlockchain::new()
        }
    }

    #[async_trait::async_trait]
    impl Blockchain for MockBlockchain {
        async fn balance(&self, key: Address) -> Result<u64> {
            self.mock_balance.call(key)
        }

        async fn issue_create_request(&self, request: CreateRequest) -> Result<Receipt> {
            self.mock_create_request.call(request)
        }

        async fn redeem(&self, request: RedeemRequest) -> Result<Receipt> {
            self.mock_redeem_request.call(request)
        }

        async fn send_claims(&self, request: SendClaimsRequest) -> Result<Receipt> {
            self.mock_fund_claims_request.call(request)
        }

        async fn check_connectivity(&self) -> Result<()> {
            self.mock_check_connectivity.call(())
        }
    }
}
