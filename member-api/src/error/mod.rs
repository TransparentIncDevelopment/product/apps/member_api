pub mod utils;

use crate::{
    accounts::repository::errors::AccountsRepositoryError,
    banks::{
        api::errors::{BalanceError, FundClaimsError, XandBanksClientConstructionError},
        repository::errors::BanksRepositoryError,
    },
    config::{ConfigurationError, SecretStoreCreationError},
    health::banks::BanksServiceCreationError,
    transaction::mapper::TransactionMappingError,
};
use snafu::Snafu;
use xand_address::AddressError;
use xand_api_client::{errors::XandApiClientError, TonicCode};
use xand_secrets::{CheckHealthError, ReadSecretError};

#[allow(clippy::large_enum_variant)]
#[derive(Clone, Debug, Serialize, Snafu)]
#[snafu(visibility(pub))]
pub enum Error {
    #[snafu(display("Account repository error: {:?}", source))]
    AccountRepositoryErrors {
        #[snafu(source)]
        source: AccountsRepositoryError,
    },

    #[snafu(display("Bank repository error: {:?}", source))]
    BankRepositoryErrors {
        #[snafu(source)]
        source: BanksRepositoryError,
    },

    #[snafu(display("{}", message))]
    Configuration { message: String },

    #[snafu(display("{}", message))]
    Serialization { message: String },

    #[snafu(display("{}", source))]
    TransactionMapping { source: TransactionMappingError },

    #[snafu(display("{}", source))]
    Utils {
        #[snafu(source)]
        source: utils::UtilsError,
    },

    #[snafu(display("{}", message))]
    Validation { message: String },

    #[snafu(display("Error in xand api client during {}: {}", message, source))]
    XandClientError {
        message: String,
        source: XandApiClientError,
    },

    XandBanks {
        #[snafu(source)]
        source: xand_banks::errors::XandBanksErrors,
    },

    #[snafu(display("{}", message))]
    Generic { message: String },

    #[snafu(display("NotFound: {}", message))]
    NotFound { message: String },

    #[snafu(display("FormatError: {}", message))]
    Format { message: String, input: String },

    #[snafu(display("Request Timeout"))]
    RequestTimeout,

    #[snafu(display("Invalid address in request: {}", source))]
    #[snafu(context(false))]
    InvalidAddress { source: AddressError },

    #[snafu(display("IO Error: {}", message))]
    IoError { message: String },

    #[snafu(display("Unauthorized: {}", message))]
    Unauthorized { message: String },

    #[snafu(display("Database Error: {}", message))]
    Database { message: String },

    #[snafu(display("Secret store error: {}", message))]
    SecretStore { message: String },

    #[snafu(display("Bank Health Service Error: {}", source))]
    BanksHealthService { source: BanksServiceCreationError },
    #[snafu(display(
        "Failure casting {} from type {} to type {}",
        value,
        original_type,
        target_type
    ))]
    PrimitiveCastFailure {
        value: String,
        original_type: String,
        target_type: String,
    },
}

impl From<ReadSecretError> for Error {
    fn from(source: ReadSecretError) -> Self {
        Error::SecretStore {
            message: format!("{:#?}", source),
        }
    }
}

impl From<SecretStoreCreationError> for Error {
    fn from(source: SecretStoreCreationError) -> Self {
        Error::SecretStore {
            message: format!("{}", source),
        }
    }
}

impl From<ConfigurationError> for Error {
    fn from(source: ConfigurationError) -> Self {
        Error::Configuration {
            message: format!("{}", source),
        }
    }
}

impl From<CheckHealthError> for Error {
    fn from(source: CheckHealthError) -> Self {
        Error::SecretStore {
            message: format!("{}", source),
        }
    }
}

impl From<std::io::Error> for Error {
    fn from(io_err: std::io::Error) -> Self {
        Error::IoError {
            message: io_err.to_string(),
        }
    }
}

impl From<XandApiClientError> for Error {
    fn from(api_error: XandApiClientError) -> Self {
        match api_error.clone() {
            XandApiClientError::TransportError { .. } => Error::XandClientError {
                message: "Xand API Client Transport Error".to_string(),
                source: api_error,
            },

            XandApiClientError::ProtoError { .. } => Error::XandClientError {
                message: "Xand API Client Proto Error".to_string(),
                source: api_error,
            },

            XandApiClientError::InvalidIdInTransaction { .. } => Error::XandClientError {
                message: "Xand API Client Invalid ID In TX Error".to_string(),
                source: api_error,
            },

            XandApiClientError::BadReplyError { .. } => Error::XandClientError {
                message: "Xand API Client Bad Reply Error".to_string(),
                source: api_error,
            },

            XandApiClientError::NotFound { message } => Error::NotFound { message },

            XandApiClientError::BadRequest { message } => Error::XandClientError {
                message,
                source: api_error,
            },

            XandApiClientError::OtherGrpcError { source } => match source.code() {
                TonicCode::NotFound => Error::NotFound {
                    message: source.message().to_string(),
                },
                _ => Error::XandClientError {
                    message: source.message().to_string(),
                    source: api_error,
                },
            },

            XandApiClientError::UnknownTransactionStatus => Error::XandClientError {
                message: api_error.to_string(),
                source: api_error,
            },

            XandApiClientError::InvalidAddress { .. } => Error::Generic {
                message: format!("Invalid Address: {}", api_error),
            },

            XandApiClientError::InvalidCidrBlock { .. } => Error::Generic {
                message: format!("Invalid Cidr Block: {}", api_error),
            },

            XandApiClientError::Timeout => Error::RequestTimeout {},

            XandApiClientError::Unauthorized { message } => Error::XandClientError {
                message: format!("Member API Can't authenticate to the XAND API: {}", message),
                source: api_error,
            },
        }
    }
}

impl From<utils::UtilsError> for Error {
    fn from(source: utils::UtilsError) -> Self {
        Error::Utils { source }
    }
}

impl From<xand_banks::errors::XandBanksErrors> for Error {
    fn from(source: xand_banks::errors::XandBanksErrors) -> Self {
        Error::XandBanks { source }
    }
}

impl From<serde_json::error::Error> for Error {
    fn from(e: serde_json::error::Error) -> Self {
        Error::Serialization {
            message: e.to_string(),
        }
    }
}

impl From<std::string::FromUtf8Error> for Error {
    fn from(e: std::string::FromUtf8Error) -> Self {
        Error::Serialization {
            message: e.to_string(),
        }
    }
}

impl From<BanksServiceCreationError> for Error {
    fn from(source: BanksServiceCreationError) -> Self {
        Error::BanksHealthService { source }
    }
}

impl From<XandBanksClientConstructionError> for crate::error::Error {
    fn from(source: XandBanksClientConstructionError) -> Self {
        Error::Generic {
            message: source.message,
        }
    }
}

impl From<FundClaimsError> for crate::error::Error {
    fn from(source: FundClaimsError) -> Self {
        Error::Generic {
            message: source.message,
        }
    }
}

impl From<BalanceError> for crate::error::Error {
    fn from(source: BalanceError) -> Self {
        Error::Generic {
            message: source.message,
        }
    }
}

impl Error {
    pub fn numeric_cast_failure<
        Original: num_traits::PrimInt + std::fmt::Display,
        Target: num_traits::PrimInt,
    >(
        original_value: Original,
    ) -> Self {
        Error::PrimitiveCastFailure {
            value: original_value.to_string(),
            original_type: std::any::type_name::<Original>().to_string(),
            target_type: std::any::type_name::<Target>().to_string(),
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use xand_api_client::TonicStatus;

    #[test]
    fn not_found_grpc_xand_api_error_to_member_api_error() {
        let xand_error = XandApiClientError::OtherGrpcError {
            source: TonicStatus::new(TonicCode::NotFound, "Error Message.".to_string()).into(),
        };

        let error: crate::Error = xand_error.into();

        let message = match error {
            crate::Error::NotFound { message } => message,
            _ => "Ignore".to_string(),
        };

        assert_eq!(message, "Error Message.".to_string());
    }

    #[test]
    fn not_found_first_class_xand_api_error_to_member_api_error() {
        let xand_error = XandApiClientError::NotFound {
            message: "Error Message.".to_string(),
        };

        let error: crate::Error = xand_error.into();

        let message = match error {
            crate::Error::NotFound { message } => message,
            _ => "Ignore".to_string(),
        };

        assert_eq!(message, "Error Message.".to_string());
    }

    #[test]
    fn xand_api_unauthorized() {
        let input = XandApiClientError::Unauthorized {
            message: "This is a test".to_string(),
        };
        let result: Error = input.into();
        assert!(matches!(
            result,
            Error::XandClientError {
                message, source: _
            } if message == "Member API Can't authenticate to the XAND API: This is a test"
        ));
    }
}
