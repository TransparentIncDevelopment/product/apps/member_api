use snafu::Snafu;
use std::sync::Arc;

#[derive(Clone, Debug, Serialize, Snafu)]
#[snafu(visibility(pub))] // Required here or else must be declared in using module
pub enum UtilsError {
    #[snafu(display("Getting token path: {:?}", source))]
    CreateAccessTokenPath {
        #[snafu(source(from(std::io::Error, Arc::new)))]
        #[serde(serialize_with = "xand_utils::snafu_extensions::debug_serialize")]
        source: Arc<std::io::Error>,
    },
    #[snafu(display("Getting token path: {}", message))]
    OsCreateAccessTokenPath {
        message: String,
    },
    Generic,
}
