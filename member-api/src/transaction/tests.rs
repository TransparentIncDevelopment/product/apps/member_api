use xand_address::Address;
use xand_api_client::{
    BankAccountId, BankAccountInfo, CashConfirmation, CorrelationId, CreateRequestCompletion,
    EncryptionError, PendingCreateRequest, PendingRedeemRequest, RedeemFulfillment,
    RedeemRequestCompletion, Send, TransactionId, TransactionStatus, XandTransaction,
};

use crate::{
    accounts::{testing::MockAccountsRepository, web::ApiAccount, AccountDetails},
    tests::TestDataBuilder,
    transaction::{
        correlation_id::correlation_id_from_transaction,
        mapper::{TransactionMapper, TransactionMappingError},
        Status, Transaction, TransactionState, TransactionType,
    },
};

use chrono::{naive::NaiveDate, DateTime, Utc};

use std::str::FromStr;
use xand_api_client::TransactionHistoryPage;

struct TestContext {
    mapper: TransactionMapper,
    accounts: MockAccountsRepository,
}

impl TestContext {
    fn map(
        &self,
        input: xand_api_client::Transaction,
    ) -> Result<Transaction, TransactionMappingError> {
        let history = TransactionHistoryPage {
            total: 1,
            data: vec![input],
        };

        let result = self.mapper.map_from_xand_history(&history)?;
        Ok(result.transactions[0].clone())
    }

    fn setup_account(&mut self, account: AccountDetails) {
        self.accounts.mock_get_all.return_ok(vec![account])
    }
}

impl Default for TestContext {
    fn default() -> Self {
        let accounts = MockAccountsRepository::default();
        let mapper = TransactionMapper {
            accounts: Box::new(accounts.clone()),
        };

        TestContext { mapper, accounts }
    }
}

#[test]
fn map_create_pending() -> Result<(), TransactionMappingError> {
    let mut context = TestContext::default();

    let fredbob = AccountDetails::build(2);

    context.setup_account(fredbob.clone());

    let txn = XandTransaction::CreateRequest(PendingCreateRequest {
        amount_in_minor_unit: 42,
        account: fredbob.clone().into(),
        correlation_id: CorrelationId::gen_random(),
        completing_transaction: None,
    });
    let timestamp = DateTime::from_utc(NaiveDate::from_ymd(2020, 2, 5).and_hms(6, 0, 0), Utc);
    let input = xand_api_client::Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        "5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"
            .parse()
            .unwrap(),
        TransactionStatus::Committed,
        timestamp,
    );

    let result = context.map(input.clone())?;

    let correlation_id: String = correlation_id_from_transaction(&input);

    assert_eq!(TransactionType::CreateRequest, result.operation);
    assert_eq!(input.signer_address.to_string(), result.signer_address);
    assert_eq!(input.timestamp, result.datetime);
    assert_eq!(result.transaction_id, input.transaction_id.to_string());
    assert_eq!(result.correlation_id, correlation_id);
    assert_eq!(result.amount_in_minor_unit, 42);
    assert_eq!(
        result.destination_address,
        input.get_destination_addr().unwrap().to_string()
    );
    assert_eq!(result.bank_account, Some(fredbob.into()));
    assert_eq!(
        result.status,
        Status {
            state: TransactionState::Pending,
            details: None
        }
    );
    assert_eq!(result.confirmation_id, "".to_string());
    assert_eq!(result.cancellation_id, "".to_string());

    Ok(())
}

#[test]
fn map_create_confirmed() -> Result<(), TransactionMappingError> {
    let mut context = TestContext::default();

    let fredbob = AccountDetails::build(2);

    context.setup_account(fredbob.clone());

    let confirmation_id: TransactionId =
        "0x1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef"
            .parse()
            .unwrap();
    let confirmation = Some(CreateRequestCompletion::Confirmation(
        confirmation_id.clone(),
    ));

    let txn = XandTransaction::CreateRequest(PendingCreateRequest {
        amount_in_minor_unit: 42,
        account: fredbob.into(),
        correlation_id: CorrelationId::gen_random(),
        completing_transaction: confirmation,
    });

    let timestamp = DateTime::from_utc(NaiveDate::from_ymd(2020, 2, 5).and_hms(6, 0, 0), Utc);
    let input = xand_api_client::Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
        TransactionStatus::Committed,
        timestamp,
    );

    let result = context.map(input)?;

    assert_eq!(result.confirmation_id, confirmation_id.to_string());
    assert_eq!(result.cancellation_id, "".to_string());

    Ok(())
}

#[test]
fn map_create_cancelled() -> Result<(), TransactionMappingError> {
    let mut context = TestContext::default();

    let fredbob = AccountDetails::build(2);

    context.setup_account(fredbob.clone());

    let cancellation_id: TransactionId =
        "0x1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef"
            .parse()
            .unwrap();
    let cancellation = Some(CreateRequestCompletion::Cancellation(
        cancellation_id.clone(),
    ));

    let txn = XandTransaction::CreateRequest(PendingCreateRequest {
        amount_in_minor_unit: 42,
        account: fredbob.into(),
        correlation_id: CorrelationId::gen_random(),
        completing_transaction: cancellation,
    });

    let timestamp = DateTime::from_utc(NaiveDate::from_ymd(2020, 2, 5).and_hms(6, 0, 0), Utc);
    let input = xand_api_client::Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
        TransactionStatus::Committed,
        timestamp,
    );

    let result = context.map(input)?;

    assert_eq!(result.confirmation_id, "".to_string());
    assert_eq!(result.cancellation_id, cancellation_id.to_string());

    Ok(())
}

#[test]
fn map_create_expired() -> Result<(), TransactionMappingError> {
    let mut context = TestContext::default();

    let fredbob = AccountDetails::build(2);

    context.setup_account(fredbob.clone());

    let cancellation_id: TransactionId =
        "0x1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef"
            .parse()
            .unwrap();
    let expiration = Some(CreateRequestCompletion::Expiration(cancellation_id.clone()));

    let txn = XandTransaction::CreateRequest(PendingCreateRequest {
        amount_in_minor_unit: 42,
        account: fredbob.into(),
        correlation_id: CorrelationId::gen_random(),
        completing_transaction: expiration,
    });

    let timestamp = DateTime::from_utc(NaiveDate::from_ymd(2020, 2, 5).and_hms(6, 0, 0), Utc);
    let input = xand_api_client::Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
        TransactionStatus::Committed,
        timestamp,
    );

    let result = context.map(input)?;

    assert_eq!(result.confirmation_id, "".to_string());
    assert_eq!(result.cancellation_id, cancellation_id.to_string());

    Ok(())
}

#[test]
fn map_create_with_unknown_bank() -> Result<(), TransactionMappingError> {
    let mut context = TestContext::default();

    let fredbob = AccountDetails::build(2);
    let robpaul = AccountDetails::build(3);

    context.setup_account(robpaul);

    let txn = XandTransaction::CreateRequest(PendingCreateRequest {
        amount_in_minor_unit: 42,
        account: fredbob.into(),
        correlation_id: CorrelationId::gen_random(),
        completing_transaction: None,
    });

    let timestamp = DateTime::from_utc(NaiveDate::from_ymd(2020, 2, 5).and_hms(6, 0, 0), Utc);
    let input = xand_api_client::Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
        TransactionStatus::Committed,
        timestamp,
    );

    let result = context.map(input)?;

    assert_eq!(result.bank_account, Some(ApiAccount::unresolvable()));
    Ok(())
}

#[test]
fn map_create_with_undecryptable_bank() -> Result<(), TransactionMappingError> {
    let mut context = TestContext::default();
    let robpaul = AccountDetails::build(3);

    context.setup_account(robpaul);

    let txn = XandTransaction::CreateRequest(PendingCreateRequest {
        amount_in_minor_unit: 42,
        account: BankAccountInfo::Encrypted(EncryptionError::KeyNotFound),
        // TODO - https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/6180 - Consider creating different types for request and response types rather than forcing caller to submit a payload containing an Error
        correlation_id: CorrelationId::gen_random(),
        completing_transaction: None,
    });

    let timestamp = DateTime::from_utc(NaiveDate::from_ymd(2020, 2, 5).and_hms(6, 0, 0), Utc);
    let input = xand_api_client::Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
        TransactionStatus::Committed,
        timestamp,
    );

    let result = context.map(input)?;

    assert_eq!(result.bank_account, Some(ApiAccount::unresolvable()));
    Ok(())
}

#[test]
fn map_redeem_with_unknown_bank() -> Result<(), TransactionMappingError> {
    let mut context = TestContext::default();

    let fredbob = AccountDetails::build(2);
    let robpaul = AccountDetails::build(3);

    context.setup_account(robpaul);

    let txn = XandTransaction::RedeemRequest(PendingRedeemRequest {
        amount_in_minor_unit: 42,
        account: fredbob.into(),
        correlation_id: CorrelationId::gen_random(),
        completing_transaction: None,
    });

    let timestamp = DateTime::from_utc(NaiveDate::from_ymd(2020, 2, 5).and_hms(6, 0, 0), Utc);
    let input = xand_api_client::Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
        TransactionStatus::Committed,
        timestamp,
    );

    let result = context.map(input)?;

    assert_eq!(result.bank_account, Some(ApiAccount::unresolvable()));
    Ok(())
}

#[test]
fn map_redeem_with_undecryptable_bank() -> Result<(), TransactionMappingError> {
    let mut context = TestContext::default();
    let robpaul = AccountDetails::build(3);

    context.setup_account(robpaul);

    let txn = XandTransaction::RedeemRequest(PendingRedeemRequest {
        amount_in_minor_unit: 42,
        account: BankAccountInfo::Encrypted(EncryptionError::KeyNotFound),
        correlation_id: CorrelationId::gen_random(),
        completing_transaction: None,
    });

    let timestamp = DateTime::from_utc(NaiveDate::from_ymd(2020, 2, 5).and_hms(6, 0, 0), Utc);
    let input = xand_api_client::Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
        TransactionStatus::Committed,
        timestamp,
    );

    let result = context.map(input)?;

    assert_eq!(result.bank_account, Some(ApiAccount::unresolvable()));
    Ok(())
}

#[test]
fn map_redeem_with_unencrypted_bank() -> Result<(), TransactionMappingError> {
    let mut context = TestContext::default();

    let fredbob = AccountDetails::build(2);

    context.setup_account(fredbob.clone());

    let confirmation_id: TransactionId =
        "0x1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef"
            .parse()
            .unwrap();
    let confirmation = Some(RedeemRequestCompletion::Confirmation(
        confirmation_id.clone(),
    ));
    let account_number = fredbob.account_number;
    let routing_number = fredbob.routing_number;

    let txn = XandTransaction::RedeemRequest(PendingRedeemRequest {
        amount_in_minor_unit: 42,
        account: BankAccountInfo::Unencrypted(BankAccountId {
            routing_number,
            account_number,
        }),
        correlation_id: CorrelationId::gen_random(),
        completing_transaction: confirmation,
    });

    let timestamp = DateTime::from_utc(NaiveDate::from_ymd(2020, 2, 5).and_hms(6, 0, 0), Utc);
    let input = xand_api_client::Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
        TransactionStatus::Committed,
        timestamp,
    );

    let result = context.map(input)?;

    assert_eq!(result.confirmation_id, confirmation_id.to_string());
    assert_eq!(result.cancellation_id, "".to_string());

    Ok(())
}

#[test]
fn map_transfer_none_bank() {
    let context = TestContext::default();
    context.accounts.mock_get_all.return_ok(vec![]);

    let txn = XandTransaction::Send(Send {
        amount_in_minor_unit: 42,
        destination_account: Address::from_str("5EgqcJ1sbFkUvySAmxc3bThRePvdmw3zCrXMjqYPaSSSzbkQ")
            .unwrap(),
    });

    let timestamp = DateTime::from_utc(NaiveDate::from_ymd(2020, 2, 5).and_hms(6, 0, 0), Utc);
    let input = xand_api_client::Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
        TransactionStatus::Invalid("invalidboi".to_string()),
        timestamp,
    );

    let result = context.map(input).unwrap();
    assert_eq!(result.bank_account, None);
}

#[test]
fn map_create_none_bank() -> Result<(), TransactionMappingError> {
    let context = TestContext::default();
    context.accounts.mock_get_all.return_ok(vec![]);

    let txn = XandTransaction::CashConfirmation(CashConfirmation {
        correlation_id: CorrelationId::gen_random(),
    });

    let timestamp = DateTime::from_utc(NaiveDate::from_ymd(2020, 2, 5).and_hms(6, 0, 0), Utc);
    let input = xand_api_client::Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
        TransactionStatus::Committed,
        timestamp,
    );

    let result = context.map(input)?;

    assert_eq!(result.bank_account, None);

    Ok(())
}

#[test]
fn map_redeem_fulfillment_none_bank() -> Result<(), TransactionMappingError> {
    let context = TestContext::default();
    context.accounts.mock_get_all.return_ok(vec![]);

    let txn = XandTransaction::RedeemFulfillment(RedeemFulfillment {
        correlation_id: CorrelationId::gen_random(),
    });

    let timestamp = DateTime::from_utc(NaiveDate::from_ymd(2020, 2, 5).and_hms(6, 0, 0), Utc);
    let input = xand_api_client::Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
        TransactionStatus::Committed,
        timestamp,
    );

    let result = context.map(input)?;

    assert_eq!(result.bank_account, None);

    Ok(())
}

#[test]
fn map_send() {
    let context = TestContext::default();
    context.accounts.mock_get_all.return_ok(vec![]);

    let txn = XandTransaction::Send(Send {
        amount_in_minor_unit: 42,
        destination_account: Address::from_str("5EgqcJ1sbFkUvySAmxc3bThRePvdmw3zCrXMjqYPaSSSzbkQ")
            .unwrap(),
    });

    let timestamp = DateTime::from_utc(NaiveDate::from_ymd(2020, 2, 5).and_hms(6, 0, 0), Utc);
    let input = xand_api_client::Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
        TransactionStatus::Invalid("invalidboi".to_string()),
        timestamp,
    );

    let result = context.map(input.clone()).unwrap();

    assert_eq!(result.operation, TransactionType::Payment);
    assert_eq!(input.signer_address.to_string(), result.signer_address);
    assert_eq!(input.timestamp, result.datetime);
    assert_eq!(result.transaction_id, input.transaction_id.to_string());
    assert_eq!(result.correlation_id, "".to_string());
    assert_eq!(result.amount_in_minor_unit, 42);
    assert_eq!(
        &result.destination_address,
        "5EgqcJ1sbFkUvySAmxc3bThRePvdmw3zCrXMjqYPaSSSzbkQ"
    );
    assert_eq!(
        result.status,
        Status {
            state: TransactionState::Invalid,
            details: Some("invalidboi".to_string())
        }
    );
    assert_eq!(result.confirmation_id, "".to_string());
    assert_eq!(result.cancellation_id, "".to_string());
}

#[test]
fn map_redeem() -> Result<(), TransactionMappingError> {
    let mut context = TestContext::default();
    let fredbob = AccountDetails::build(2);

    context.setup_account(fredbob.clone());

    let txn = XandTransaction::RedeemRequest(PendingRedeemRequest {
        amount_in_minor_unit: 42,
        account: fredbob.clone().into(),
        correlation_id: CorrelationId::gen_random(),
        completing_transaction: None,
    });

    let timestamp = DateTime::from_utc(NaiveDate::from_ymd(2020, 2, 5).and_hms(6, 0, 0), Utc);
    let input = xand_api_client::Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
        TransactionStatus::Committed,
        timestamp,
    );

    let result = context.map(input.clone())?;

    let correlation_id: String = correlation_id_from_transaction(&input);

    assert_eq!(result.operation, TransactionType::Redeem);
    assert_eq!(input.signer_address.to_string(), result.signer_address);
    assert_eq!(result.transaction_id, input.transaction_id.to_string());
    assert_eq!(input.timestamp, result.datetime);
    assert_eq!(result.correlation_id, correlation_id);
    assert_eq!(result.amount_in_minor_unit, 42);
    assert_eq!(result.destination_address, "");
    assert_eq!(result.bank_account, Some(fredbob.into()));
    assert_eq!(
        result.status,
        Status {
            state: TransactionState::Pending,
            details: None
        }
    );
    assert_eq!(result.confirmation_id, "".to_string());
    assert_eq!(result.cancellation_id, "".to_string());

    Ok(())
}

#[test]
fn map_redeem_confirmed() -> Result<(), TransactionMappingError> {
    let mut context = TestContext::default();
    let fredbob = AccountDetails::build(2);

    context.setup_account(fredbob.clone());

    let confirmation_id: TransactionId =
        "0x1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef"
            .parse()
            .unwrap();
    let confirmation = Some(RedeemRequestCompletion::Confirmation(
        confirmation_id.clone(),
    ));

    let txn = XandTransaction::RedeemRequest(PendingRedeemRequest {
        amount_in_minor_unit: 42,
        account: fredbob.clone().into(),
        correlation_id: CorrelationId::gen_random(),
        completing_transaction: confirmation,
    });

    let timestamp = DateTime::from_utc(NaiveDate::from_ymd(2020, 2, 5).and_hms(6, 0, 0), Utc);
    let input = xand_api_client::Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
        TransactionStatus::Committed,
        timestamp,
    );

    let result = context.map(input.clone())?;

    let correlation_id: String = correlation_id_from_transaction(&input);

    assert_eq!(result.operation, TransactionType::Redeem);
    assert_eq!(input.signer_address.to_string(), result.signer_address);
    assert_eq!(input.timestamp, result.datetime);
    assert_eq!(result.transaction_id, input.transaction_id.to_string());
    assert_eq!(result.correlation_id, correlation_id);
    assert_eq!(result.amount_in_minor_unit, 42);
    assert_eq!(result.destination_address, "");
    assert_eq!(result.bank_account, Some(fredbob.into()));
    assert_eq!(
        result.status,
        Status {
            state: TransactionState::Confirmed,
            details: None
        }
    );
    assert_eq!(result.confirmation_id, confirmation_id.to_string());
    assert_eq!(result.cancellation_id, "".to_string());

    Ok(())
}

#[test]
fn map_redeem_cancelled() -> Result<(), TransactionMappingError> {
    let mut context = TestContext::default();
    let fredbob = AccountDetails {
        id: 4,
        bank_id: 2,
        bank_name: "Gringott's".to_string(),
        short_name: "Frederick Roberts".to_string(),
        routing_number: "9876543210".to_string(),
        account_number: "01234567890".to_string(),
    };

    context.setup_account(fredbob.clone());

    let cancellation_id: TransactionId =
        "0x1234567890abcdef1234567890abcdef1234567890abcdef1234567890abcdef"
            .parse()
            .unwrap();
    let cancellation = Some(RedeemRequestCompletion::Cancellation(
        cancellation_id.clone(),
    ));

    let txn = XandTransaction::RedeemRequest(PendingRedeemRequest {
        amount_in_minor_unit: 42,
        account: fredbob.into(),
        correlation_id: CorrelationId::gen_random(),
        completing_transaction: cancellation,
    });

    let timestamp = DateTime::from_utc(NaiveDate::from_ymd(2020, 2, 5).and_hms(6, 0, 0), Utc);
    let input = xand_api_client::Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
        TransactionStatus::Committed,
        timestamp,
    );

    let result = context.map(input)?;
    assert_eq!(
        result.status,
        Status {
            state: TransactionState::Cancelled,
            details: None
        }
    );
    assert_eq!(result.confirmation_id, "".to_string());
    assert_eq!(result.cancellation_id, cancellation_id.to_string());

    Ok(())
}

#[test]
fn map_create() -> Result<(), TransactionMappingError> {
    let context = TestContext::default();
    context.accounts.mock_get_all.return_ok(vec![]);

    let txn = XandTransaction::CashConfirmation(CashConfirmation {
        correlation_id: CorrelationId::gen_random(),
    });

    let timestamp = DateTime::from_utc(NaiveDate::from_ymd(2020, 2, 5).and_hms(6, 0, 0), Utc);
    let input = xand_api_client::Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
        TransactionStatus::Committed,
        timestamp,
    );

    let result = context.map(input.clone())?;

    let correlation_id: String = correlation_id_from_transaction(&input);

    assert_eq!(result.operation, TransactionType::Create);
    assert_eq!(input.signer_address.to_string(), result.signer_address);
    assert_eq!(input.timestamp, result.datetime);
    assert_eq!(result.transaction_id, input.transaction_id.to_string());
    assert_eq!(result.correlation_id, correlation_id);
    assert_eq!(result.amount_in_minor_unit, 0);
    assert_eq!(result.destination_address, "");
    assert_eq!(
        result.status,
        Status {
            state: TransactionState::Confirmed,
            details: None
        }
    );

    Ok(())
}

#[test]
fn map_redeem_fulfillment() -> Result<(), TransactionMappingError> {
    let context = TestContext::default();
    context.accounts.mock_get_all.return_ok(vec![]);

    let txn = XandTransaction::RedeemFulfillment(RedeemFulfillment {
        correlation_id: CorrelationId::gen_random(),
    });

    let timestamp = DateTime::from_utc(NaiveDate::from_ymd(2020, 2, 5).and_hms(6, 0, 0), Utc);
    let input = xand_api_client::Transaction::from_xand_txn(
        TransactionId::default(),
        txn,
        Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
        TransactionStatus::Committed,
        timestamp,
    );

    let result = context.map(input.clone())?;

    let correlation_id: String = correlation_id_from_transaction(&input);

    assert_eq!(result.operation, TransactionType::RedeemFulfillment);
    assert_eq!(input.signer_address.to_string(), result.signer_address);
    assert_eq!(input.timestamp, result.datetime);
    assert_eq!(result.transaction_id, input.transaction_id.to_string());
    assert_eq!(result.correlation_id, correlation_id);
    assert_eq!(result.amount_in_minor_unit, 0);
    assert_eq!(result.destination_address, "");
    assert_eq!(
        result.status,
        Status {
            state: TransactionState::Confirmed,
            details: None
        }
    );

    Ok(())
}
