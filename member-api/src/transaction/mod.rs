use std::sync::Arc;

use crate::{error::XandClientError, transaction::mapper::TransactionMapper};
use chrono::{DateTime, Utc};
use snafu::ResultExt;
use uuid::Uuid;
use xand_address::Address;

use crate::accounts::web::ApiAccount;
#[cfg(test)]
mod tests;

mod correlation_id;
pub mod mapper;

#[cfg(test)]
pub mod mocks;

#[cfg(test)]
mod builders;

/// Reference metadata part of the TX submitted by the user
#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
#[serde(rename_all = "camelCase")]
pub struct Reference {
    /// User submitted reference ID
    pub id: String,
    /// User submitted datetime
    pub datetime: DateTime<Utc>,
}

use xand_api_client::{
    errors::XandApiClientError, models::Paging, TransactionFilter, TransactionId,
    TransactionType as PModelsTransactionType, XandApiClientTrait, XandTransaction,
};

impl Default for Reference {
    fn default() -> Reference {
        Reference {
            id: Uuid::new_v4().to_string(),
            datetime: Utc::now(),
        }
    }
}

pub struct TransactionHistoryRequest {
    pub address: Address,
    pub size: usize,
    pub page: usize,
}

/// Repository implementation for banks and bank accounts.
#[async_trait::async_trait]
pub trait TransactionRepository: dyn_clone::DynClone + Send + Sync {
    async fn get(&self, id: String) -> Result<Transaction, crate::Error>;

    async fn get_history(
        &self,
        request: TransactionHistoryRequest,
    ) -> Result<TransactionHistory, crate::Error>;
}
dyn_clone::clone_trait_object!(TransactionRepository);

#[derive(Clone, new)]
pub struct TransactionRepositoryImpl {
    pub client: Arc<dyn XandApiClientTrait>,
    pub mapper: TransactionMapper,
}

#[async_trait::async_trait]
impl TransactionRepository for TransactionRepositoryImpl {
    async fn get(&self, id: String) -> Result<Transaction, crate::Error> {
        let transaction_id: TransactionId = id.parse()?;

        let xand_transaction = self
            .client
            .get_transaction_details(&transaction_id)
            .await
            .map_err(|error| match error {
                XandApiClientError::NotFound { .. } => crate::Error::NotFound {
                    message: format!("Transaction with ID {} could not be found.", transaction_id),
                },
                XandApiClientError::BadRequest { message } => crate::Error::Validation { message },
                _ => error.into(),
            })?;

        let transaction = self.mapper.map_from_xand_model(&xand_transaction);

        Ok(transaction)
    }

    async fn get_history(
        &self,
        request: TransactionHistoryRequest,
    ) -> Result<TransactionHistory, crate::Error> {
        let mapper = self.mapper.clone();

        let transaction_history = self
            .client
            .get_transaction_history(
                Some(Paging {
                    page_size: request.size as u32,
                    page_number: request.page as u32,
                }),
                &TransactionFilter {
                    addresses: vec![request.address.clone()],
                    types: vec![
                        PModelsTransactionType::Send,
                        PModelsTransactionType::CreateRequest,
                        PModelsTransactionType::CreateCancellation,
                        PModelsTransactionType::RedeemRequest,
                        PModelsTransactionType::RedeemCancellation,
                    ],
                    start_time: None,
                    end_time: None,
                },
            )
            .await
            .context(XandClientError {
                message: format!(
                    "While fetching transaction history for {}",
                    request.address.clone()
                ),
            })?;
        Ok(mapper.map_from_xand_history(&transaction_history)?)
    }
}

#[derive(Clone, Debug, Deserialize, Serialize, Eq, PartialEq)]
#[serde(rename_all = "camelCase")]
pub enum TransactionType {
    Create,
    CreateRequest,
    Redeem,
    RedeemFulfillment,
    Payment,
    Register,
    General,
}

#[derive(Clone, Debug, Deserialize, Serialize, Eq, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct Status {
    pub state: TransactionState,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub details: Option<String>,
}

#[derive(Clone, Debug, Deserialize, Serialize, Eq, PartialEq)]
#[serde(rename_all = "camelCase")]
pub enum TransactionState {
    Unknown,

    /// Transaction is committed to the chain but in the case of create requests
    /// and redeems it has not yet been confirmed.
    Pending,

    /// Create requests and Redeems require a second Create or Redeem Fulfillment
    /// transaction respectively. Payments should never be in this state.
    Confirmed,

    /// The transaction was rejected by the chain.
    Invalid,

    /// In the case of create requests and redeems, the transaction has been
    /// cancelled. Payments should never be in this state.
    Cancelled,
}

#[derive(Clone, Debug, Deserialize, Serialize, Eq, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct Transaction {
    #[serde(skip_serializing_if = "String::is_empty")]
    pub transaction_id: String,
    pub operation: TransactionType,
    #[serde(skip_serializing_if = "String::is_empty")]
    pub signer_address: String,
    pub amount_in_minor_unit: u64,
    #[serde(skip_serializing_if = "String::is_empty")]
    pub destination_address: String,
    #[serde(skip_serializing_if = "String::is_empty")]
    pub correlation_id: String,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bank_account: Option<ApiAccount>,

    pub status: Status,

    pub datetime: DateTime<Utc>,

    #[serde(skip_serializing_if = "String::is_empty")]
    #[serde(default)]
    pub confirmation_id: String,

    #[serde(skip_serializing_if = "String::is_empty")]
    #[serde(default)]
    pub cancellation_id: String,
}

#[derive(Clone, Debug, Deserialize, Serialize, Eq, PartialEq)]
#[serde(rename_all = "camelCase")]
pub struct TransactionHistory {
    pub total: usize,
    pub transactions: Vec<Transaction>,
}

impl From<xand_api_client::XandTransaction> for TransactionType {
    fn from(input: xand_api_client::XandTransaction) -> Self {
        match input {
            XandTransaction::CreateRequest(_data) => TransactionType::CreateRequest,
            XandTransaction::CashConfirmation(_data) => TransactionType::Create,
            XandTransaction::Send(_data) => TransactionType::Payment,
            XandTransaction::RedeemRequest(_data) => TransactionType::Redeem,
            XandTransaction::RedeemFulfillment(_data) => TransactionType::RedeemFulfillment,
            _ => {
                TransactionType::General
                // do nothing
            }
        }
    }
}

#[cfg(test)]
mod test {
    use xand_api_client::{mock::MockXandApiClient, TonicStatus};

    use crate::accounts::testing::MockAccountsRepository;

    use super::*;

    #[tokio::test]
    async fn get_transaction_details_returns_not_found() {
        let mock_xand_api_client = MockXandApiClient::default();
        mock_xand_api_client
            .transaction_details
            .return_err(XandApiClientError::NotFound {
                message: "Not Found".to_string(),
            });

        let repo = TransactionRepositoryImpl {
            client: Arc::new(mock_xand_api_client),
            mapper: TransactionMapper {
                accounts: Box::new(MockAccountsRepository::default()),
            },
        };
        let result = repo
            .get("0x33af2b45ad86f60c786354551490a68a4ecdd9f020e8d31811a6f63287432c4e".to_string())
            .await
            .unwrap_err();
        let message = match result {
            crate::Error::NotFound { message } => message,
            _ => "Ignore".to_string(),
        };
        assert_eq!(message, "Transaction with ID 0x33af2b45ad86f60c786354551490a68a4ecdd9f020e8d31811a6f63287432c4e could not be found.".to_string());
    }

    #[tokio::test]
    async fn get_transaction_details_returns_bad_request() {
        let mock_xand_api_client = MockXandApiClient::default();
        let error_message = "Bad Request Yo".to_string();
        mock_xand_api_client
            .transaction_details
            .return_err(XandApiClientError::BadRequest {
                message: error_message.clone(),
            });

        let repo = TransactionRepositoryImpl {
            client: Arc::new(mock_xand_api_client),
            mapper: TransactionMapper {
                accounts: Box::new(MockAccountsRepository::default()),
            },
        };
        let result = repo
            .get("0x33af2b45ad86f60c786354551490a68a4ecdd9f020e8d31811a6f63287412345".to_string())
            .await
            .unwrap_err();
        let message = match result {
            crate::Error::Validation { message, .. } => message,
            _ => "Ignore".to_string(),
        };
        assert_eq!(message, error_message);
    }

    #[tokio::test]
    async fn get_transaction_details_returns_other_grpc_error() {
        let mock_xand_api_client = MockXandApiClient::default();

        mock_xand_api_client
            .transaction_details
            .return_err(XandApiClientError::OtherGrpcError {
                source: TonicStatus::unimplemented("Not implemented").into(),
            });

        let repo = TransactionRepositoryImpl {
            client: Arc::new(mock_xand_api_client),
            mapper: TransactionMapper {
                accounts: Box::new(MockAccountsRepository::default()),
            },
        };
        let result = repo
            .get("0x33af2b45ad86f60c786354551490a68a4ecdd9f020e8d31811a6f63287412345".to_string())
            .await
            .unwrap_err();

        assert!(matches!(
            result,
            crate::Error::XandClientError {
                message,
                source: XandApiClientError::OtherGrpcError { .. },
            } if message == "Not implemented"
        ));
    }
}
