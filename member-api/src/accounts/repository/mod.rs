pub mod errors;
pub mod sqlite;

use errors::AccountsRepositoryResult;

use super::{Account, AccountDetails, BankAccountConnectionData};

pub trait AccountsRepository: dyn_clone::DynClone + Send + Sync {
    /// updates item in repository
    /// Returns the updated item
    /// Fails if the repository cannot be accessed or modified
    fn update(&self, account: Account) -> AccountsRepositoryResult<AccountDetails>;

    /// Adds a new item to the repository
    /// id field is ignored.
    /// Returns the newly created item
    /// Fails if the repository cannot be accessed or modified
    /// Fails if the item already exists
    fn insert(&self, account: Account) -> AccountsRepositoryResult<AccountDetails>;

    /// Removes an item from the repository given an id
    /// Fails if the item does not exist
    /// Fails if the repository cannot be accessed or modified
    fn remove(&self, id: i32) -> AccountsRepositoryResult<()>;
    /// Retrieves an item from the repository given an id
    /// Fails if the item does not exist
    /// Fails if the repository cannot be accessed or modified
    fn get(&self, id: i32) -> AccountsRepositoryResult<AccountDetails>;

    fn get_by_account_number(
        &self,
        routing_number: &str,
        account_number: &str,
    ) -> AccountsRepositoryResult<AccountDetails>;

    /// Retrieves all items in the repository
    /// Fails if the repository cannot be accessed or modified
    fn get_all(&self) -> AccountsRepositoryResult<Vec<AccountDetails>>;
    /// Returns a list of bank accounts in which each bank appears exactly once.
    /// The choice betwee multiple accounts within a single bank is made in an
    /// implementation-defined way.
    /// Fails if the repository cannot be accessed or modified
    fn get_single_per_bank(&self) -> AccountsRepositoryResult<Vec<AccountDetails>>;
    /// Returns an Err if the database connection fails; Ok(()) otherwise.
    fn check_health(&self) -> AccountsRepositoryResult<()>;

    fn get_bank_account_connection(
        &self,
        id: i32,
    ) -> AccountsRepositoryResult<BankAccountConnectionData>;
}
dyn_clone::clone_trait_object!(AccountsRepository);
