use std::sync::Arc;

use xand_banks::banks::adapter_config::AdapterConfig;
use xand_secrets::SecretKeyValueStore;

use crate::banks::api::{errors::XandBanksClientConstructionError, BankApi, ConnectedBankApi};

use super::BankAdapter;
use crate::accounts::repository::AccountsRepository;

pub trait BankAdapterFactory: dyn_clone::DynClone + Send + Sync {
    fn get_secrets(&self) -> Arc<dyn SecretKeyValueStore>;

    fn create(
        &self,
        adapter: BankAdapter,
    ) -> std::result::Result<BankApi, XandBanksClientConstructionError> {
        let xand_banks_adapter_config: AdapterConfig = adapter.into();
        let secrets = self.get_secrets();
        let xand_banks_api = xand_banks_adapter_config.create(secrets).map_err(|e| {
            XandBanksClientConstructionError {
                message: format!("{}", e),
            }
        })?;

        Ok(BankApi {
            adapter: xand_banks_api,
        })
    }

    fn create_from_account_id(
        &self,
        id: i32,
        accounts_repository: Box<dyn AccountsRepository>,
    ) -> std::result::Result<ConnectedBankApi, XandBanksClientConstructionError> {
        let connected_account = accounts_repository
            .get_bank_account_connection(id)
            .map_err(|e| XandBanksClientConstructionError {
                message: e.to_string(),
            })?;
        let bank_api = self.create(connected_account.clone().adapter)?;
        Ok(ConnectedBankApi {
            bank_api,
            connected_account,
        })
    }
}
dyn_clone::clone_trait_object!(BankAdapterFactory);

#[derive(Clone)]
pub struct DefaultBankAdapterFactory {
    pub secrets: Arc<dyn SecretKeyValueStore>,
}

impl BankAdapterFactory for DefaultBankAdapterFactory {
    fn get_secrets(&self) -> Arc<dyn SecretKeyValueStore> {
        self.secrets.clone()
    }
}
