use xand_banks::{models::account::TransferRequest, xand_money::Money};

use crate::banks::api::errors::{BalanceError, FundClaimsError};

use super::{BankAccountBalance, BankAdapter};
use crate::accounts::BankAccountConnectionData;

pub mod bank_adapter_factory;
pub mod errors;

#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub struct FundClaimsRequest {
    pub correlation_id: String,
    pub amount: u64,
    pub routing_number: String,
    pub account_number: String,
    pub claims_account: String,
}

#[derive(new, Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct FundClaimsResponse {
    pub amount_in_minor_unit: u64,
}

#[derive(Clone)]
pub struct BankApi {
    pub adapter: Box<dyn xand_banks::BankAdapter>,
}

impl BankApi {
    pub async fn balance(&self, account_number: &str) -> Result<BankAccountBalance, BalanceError> {
        let balance = self.adapter.balance(account_number).await;
        match balance {
            Ok(value) => {
                let available_balance_in_minor_unit = value
                    .available_balance
                    .into_i64_minor_units()
                    .map_err(|e| BalanceError {
                        message: format!(
                            "Unable to convert available balance to Usd {:?}",
                            value.available_balance
                        ),
                        inner: Box::new(e),
                    })?;

                let current_balance_in_minor_unit = value
                    .current_balance
                    .into_i64_minor_units()
                    .map_err(|e| BalanceError {
                        message: format!(
                            "Unable to convert current balance to Usd {:?}",
                            value.current_balance
                        ),
                        inner: Box::new(e),
                    })?;

                let result = BankAccountBalance {
                    current_balance_in_minor_unit,
                    available_balance_in_minor_unit,
                };
                Ok(result)
            }
            Err(e) => {
                let message = format!("Error retrieving balance for account {}", account_number);
                ::log::error!("{}: {}", message, e);

                Err(BalanceError {
                    message,
                    inner: Box::new(e),
                })
            }
        }
    }

    pub async fn fund_claims(
        &self,
        request: FundClaimsRequest,
    ) -> Result<FundClaimsResponse, FundClaimsError> {
        let amount =
            Money::from_i64_minor_units(request.amount as i64).map_err(|e| FundClaimsError {
                message: "Error creating a USD amount for claims funding request.".to_string(),
                inner: Box::new(e),
            })?;

        let cloned = request.clone();
        let mut correlation_id = cloned.correlation_id;
        // It's necessary to strip off the leading '0x' here because
        // only 32 characters are allowed in the correlation id field.accounts
        //
        if correlation_id.starts_with("0x") {
            correlation_id = correlation_id.strip_prefix("0x").unwrap().to_string();
        }

        self.adapter
            .transfer(
                TransferRequest {
                    src_account_number: cloned.account_number,
                    dst_account_number: cloned.claims_account,
                    amount,
                },
                Some(correlation_id),
            )
            .await
            .map_err(|e| {
                let message = format!(
                    "Error funding claims for account: {}; correlation id: {};",
                    request.account_number, request.correlation_id
                );
                ::log::error!("{}: {}", message, e);
                FundClaimsError {
                    message,
                    inner: Box::new(e),
                }
            })?;

        Ok(FundClaimsResponse {
            amount_in_minor_unit: request.amount,
        })
    }
}

#[derive(Clone)]
pub struct ConnectedBankApi {
    bank_api: BankApi,
    connected_account: BankAccountConnectionData,
}

impl ConnectedBankApi {
    pub async fn balance(&self) -> Result<BankAccountBalance, BalanceError> {
        self.bank_api
            .balance(self.connected_account.account_number.as_str())
            .await
    }

    pub async fn fund_claims(
        &self,
        correlation_id: String,
        amount_in_minor_units: u64,
    ) -> Result<FundClaimsResponse, FundClaimsError> {
        self.bank_api
            .fund_claims(FundClaimsRequest {
                correlation_id,
                amount: amount_in_minor_units,
                routing_number: self.connected_account.routing_number.clone(),
                account_number: self.connected_account.account_number.clone(),
                claims_account: self.connected_account.claims_account.clone(),
            })
            .await
    }
}

#[cfg(test)]
pub mod tests {
    use xand_banks::{
        models::account::TransferRequest,
        xand_money::{Money, Usd},
    };

    use crate::{accounts::testing::AccountNumbers, banks::testing::mock_adapter::MockBankAdapter};

    use super::{BankApi, FundClaimsRequest};

    #[tokio::test]
    pub async fn bank_api_trims_correlation_id_during_transfer_request() {
        let adapter = MockBankAdapter::default();
        let api = BankApi {
            adapter: Box::new(adapter.clone()),
        };

        let request = FundClaimsRequest {
            amount: 42,
            routing_number: 54321.to_routing_number(),
            account_number: 12345.to_account_number(),
            claims_account: 4314321.to_account_number(),
            correlation_id: "0x1234567890".to_string(),
        };
        let _ = api.fund_claims(request.clone()).await.unwrap();

        let amount = Usd::from_i64_minor_units(request.amount as i64).unwrap();

        let args = (
            TransferRequest {
                src_account_number: request.account_number,
                dst_account_number: request.claims_account,
                amount,
            },
            Some("1234567890".to_string()),
        );

        assert!(adapter.transfer.called_with(args));
    }
}
