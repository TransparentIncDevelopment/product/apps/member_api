#[cfg(test)]
mod tests;

use super::{errors::BanksRepositoryError, Bank};
use crate::{
    banks::repository::{
        errors::{BanksRepositoryErrorSource, BanksRepositoryResult},
        BanksRepository,
    },
    database::{
        connection_pool_builder::SqliteConnectionPool,
        error::diesel_error::DieselErrorExt,
        models::bank::{stored_bank::StoredBank, BankData},
        schema::bank::dsl::{bank as bank_table, id as id_field},
        DatabaseError,
    },
};
use diesel::{ExpressionMethods, OptionalExtension, QueryDsl, RunQueryDsl, SqliteConnection};

#[derive(Clone)]
pub struct SqliteBanksRepository {
    pub conn_pool: SqliteConnectionPool,
}

// This macro is required in order to get the id of the last account
// inserted into the table.
//
no_arg_sql_function!(
    last_insert_rowid,
    diesel::sql_types::Integer,
    "Represents the SQL last_insert_row() function"
);

impl SqliteBanksRepository {
    pub fn new(conn_pool: SqliteConnectionPool) -> Self {
        SqliteBanksRepository { conn_pool }
    }

    pub fn new_boxed(conn_pool: SqliteConnectionPool) -> Box<Self> {
        Box::new(SqliteBanksRepository::new(conn_pool))
    }

    fn map_bank(&self, value: StoredBank) -> crate::Result<Bank, DatabaseError> {
        let adapter = value.map_adapter()?;
        Ok(Bank {
            id: Some(value.id),
            name: value.name,
            routing_number: value.routing_number,
            claims_account: value.claims_account,
            adapter,
        })
    }

    pub fn get_bank_with_connection(
        &self,
        connection: &SqliteConnection,
        id: i32,
    ) -> crate::Result<Bank, DatabaseError> {
        let stored_bank: StoredBank = bank_table
            .filter(id_field.eq(id))
            .first::<StoredBank>(connection)?;
        let bank = self.map_bank(stored_bank)?;
        Ok(bank)
    }
}

impl BanksRepository for SqliteBanksRepository {
    fn update(&self, bank: Bank) -> BanksRepositoryResult<Bank> {
        if bank.id.is_none() {
            return Err(BanksRepositoryError::Validation {
                msg: "Id is required.".to_string(),
                source: BanksRepositoryErrorSource::BanksRepository,
            });
        }
        let id = bank.id.unwrap();

        self.conn_pool.connect_and_run(|connection| {
            let update_bank: BankData = bank.into();
            diesel::update(bank_table.filter(id_field.eq(id)))
                .set(&update_bank)
                .execute(connection)?;
            Ok(())
        })?;

        self.get(id)
    }

    fn insert(&self, bank: Bank) -> BanksRepositoryResult<Bank> {
        if bank.id.is_some() {
            return Err(BanksRepositoryError::Validation {
                msg: "Id is required.".to_string(),
                source: BanksRepositoryErrorSource::BanksRepository,
            });
        }

        let id = self.conn_pool.connect_and_run(|connection| {
            let new_bank: BankData = bank.into();
            diesel::insert_into(bank_table)
                .values(&new_bank)
                .execute(connection)?;

            let id = diesel::select(last_insert_rowid).get_result::<i32>(connection)?;
            Ok(id)
        })?;

        self.get(id)
    }

    fn remove(&self, id: i32) -> BanksRepositoryResult<()> {
        self.conn_pool
            .connect_and_run(|connection| {
                let affected_rows = diesel::delete(bank_table.filter(id_field.eq(id)))
                    .execute(connection)
                    .map_err(|err| match err.is_foreign_key_violation() {
                        true => DatabaseError::ValidationError {
                            msg: "Banks cannot be removed if they still have accounts.".into(),
                            is_internal_msg: false,
                        },
                        false => err.into(),
                    })?;
                if affected_rows == 0 {
                    return Err(DatabaseError::NotFound);
                }
                Ok(())
            })
            .map_err(Into::into)
    }

    fn get(&self, id: i32) -> BanksRepositoryResult<Bank> {
        let bank = self
            .conn_pool
            .connect_and_run(|connection| self.get_bank_with_connection(connection, id))?;
        Ok(bank)
    }

    fn get_all(&self) -> BanksRepositoryResult<Vec<Bank>> {
        self.conn_pool
            .connect_and_run(|connection| {
                let mut banks: Vec<Bank> = vec![];

                let stored_banks = bank_table.load::<StoredBank>(connection)?;

                for dto in stored_banks {
                    let bank = self.map_bank(dto)?;
                    banks.push(bank);
                }
                Ok(banks)
            })
            .map_err(Into::into)
    }

    fn check_health(&self) -> BanksRepositoryResult<()> {
        self.conn_pool
            .connect_and_run(|conn| {
                let _bank = bank_table.first::<StoredBank>(conn).optional()?;
                Ok(())
            })
            .map_err(Into::into)
    }
}
