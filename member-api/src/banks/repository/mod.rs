use super::Bank;
use errors::BanksRepositoryResult;

pub mod errors;
pub mod sqlite;

pub trait BanksRepository: dyn_clone::DynClone + Send + Sync {
    fn update(&self, bank: Bank) -> BanksRepositoryResult<Bank>;
    fn insert(&self, bank: Bank) -> BanksRepositoryResult<Bank>;
    fn remove(&self, id: i32) -> BanksRepositoryResult<()>;
    fn get(&self, id: i32) -> BanksRepositoryResult<Bank>;
    // Returns a vector of all banks, and a vector of any invalid bank entries that could not be translated to the domain
    fn get_all(&self) -> BanksRepositoryResult<Vec<Bank>>;
    /// Returns an Err if the database connection fails; Ok(()) otherwise.
    fn check_health(&self) -> BanksRepositoryResult<()>;
}
dyn_clone::clone_trait_object!(BanksRepository);
