use url::Url;

use xand_banks::banks::{
    adapter_config::AdapterConfig,
    mcb_adapter::config::{
        McbClientTokenRetrievalAuthConfig, McbConfig, RawAccountInfoManagerConfig,
    },
    treasury_prime_adapter::config::{TreasuryPrimeAuthConfig, TreasuryPrimeConfig},
};
use xand_banks::constants::BANK_CALL_STANDARD_TIMEOUT;

pub mod api;
pub mod repository;
#[cfg(test)]
pub mod testing;

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct Bank {
    pub id: Option<i32>,
    pub name: String,
    pub routing_number: String,
    pub claims_account: String,
    pub adapter: BankAdapter,
}

#[derive(new, Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct BankAccountBalance {
    pub current_balance_in_minor_unit: i64,
    pub available_balance_in_minor_unit: i64,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct McbAdapterConfig {
    pub url: Url,
    pub secret_user_name: String,
    pub secret_password: String,
    pub secret_client_app_ident: String,
    pub secret_organization_id: String,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct TreasuryPrimeAdapterConfig {
    pub url: Url,
    pub secret_api_key_id: String,
    pub secret_api_key_value: String,
}

#[derive(Clone, Debug, Eq, PartialEq)]
pub enum BankAdapter {
    Mcb(McbAdapterConfig),
    TreasuryPrime(TreasuryPrimeAdapterConfig),
}

impl BankAdapter {
    pub fn is_mcb(&self) -> bool {
        matches!(self, BankAdapter::Mcb(_))
    }
    pub fn is_treasury_prime(&self) -> bool {
        matches!(self, BankAdapter::TreasuryPrime(_))
    }

    pub fn get_url(&self) -> Url {
        match self {
            BankAdapter::Mcb(keys) => keys.url.clone(),
            BankAdapter::TreasuryPrime(keys) => keys.url.clone(),
        }
    }
}

impl From<BankAdapter> for AdapterConfig {
    fn from(input: BankAdapter) -> AdapterConfig {
        match input {
            BankAdapter::Mcb(adapter) => AdapterConfig::Mcb(McbConfig {
                account_info_manager: RawAccountInfoManagerConfig {
                    secret_key_client_app_ident: adapter.secret_client_app_ident,
                    secret_key_organization_id: adapter.secret_organization_id,
                },
                url: adapter.url,
                refresh_if_expiring_in_secs: None,
                auth: McbClientTokenRetrievalAuthConfig {
                    secret_key_username: adapter.secret_user_name,
                    secret_key_password: adapter.secret_password,
                },
                timeout: BANK_CALL_STANDARD_TIMEOUT,
            }),
            BankAdapter::TreasuryPrime(adapter) => {
                AdapterConfig::TreasuryPrime(TreasuryPrimeConfig {
                    url: adapter.url,
                    auth: TreasuryPrimeAuthConfig {
                        secret_key_username: adapter.secret_api_key_id,
                        secret_key_password: adapter.secret_api_key_value,
                    },
                    timeout: BANK_CALL_STANDARD_TIMEOUT,
                })
            }
        }
    }
}
