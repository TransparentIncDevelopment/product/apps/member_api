#![forbid(unsafe_code)]
#![allow(non_snake_case)]

extern crate config as cfg;
#[macro_use]
extern crate derive_new;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;
extern crate futures;
extern crate glob;
extern crate jsonwebtoken as jwt;

#[cfg(test)]
extern crate mockito;
#[cfg(test)]
extern crate pseudo;
#[macro_use]
extern crate serde;
#[cfg(test)]
extern crate toml;
#[macro_use]
extern crate tpfs_logger_port;

use crate::{
    accounts::{
        repository::{
            errors::AccountsRepositoryError, sqlite::SqliteAccountsRepository, AccountsRepository,
        },
        Account, AccountDetails,
    },
    banks::{
        api::{
            bank_adapter_factory::{BankAdapterFactory, DefaultBankAdapterFactory},
            FundClaimsResponse,
        },
        repository::{sqlite::SqliteBanksRepository, BanksRepository},
        Bank, BankAccountBalance,
    },
    config::ConfigWrapper,
    database::connection_pool_builder::ConnectionPoolBuilder,
    diagnostics::dump_fd_info,
    health::Service,
    member::Member,
};
pub use crate::{
    logging::*,
    transaction::{Transaction, TransactionHistory},
};
pub use blockchain::Receipt;
pub use error::Error;
pub use primitives::{DateTime, Paged, Result};
use std::sync::Arc;
use xand_address::Address;

pub mod accounts;
pub mod banks;
mod blockchain;
pub mod config;
mod database;
mod diagnostics;
mod error;
pub mod health;
mod logging;
#[allow(non_snake_case)]
mod member;
mod primitives;
#[cfg(test)]
mod tests;
pub mod transaction;

#[derive(Clone)]
pub struct MemberApiService {
    config: ConfigWrapper,
    bank_adapters: Box<dyn BankAdapterFactory>,
    member: Member,
    accounts_repository: Box<dyn AccountsRepository>,
    banks_repository: Box<dyn BanksRepository>,
}

impl MemberApiService {
    /// Configures the Member service with its data repositories, secret store, Xand API
    /// and bank adapters.
    pub fn from_config(config: &ConfigWrapper) -> Result<Self> {
        dump_fd_info("Before HTTP Server start.");

        let clientbox = config
            .create_xand_client()
            .expect("Could not create Xand API client");
        let xand_api_client: Arc<dyn xand_api_client::XandApiClientTrait> = clientbox.into();
        dump_fd_info("After create_xand_client");

        let secret_store = config.create_secret_store()?;

        log::debug!("Created secret store");

        let db_path = config
            .database()
            .clone()
            .into_os_string()
            .into_string()
            .unwrap();

        log::debug!("DB Path: {:#?}", db_path);

        let connection_pool = ConnectionPoolBuilder::build(db_path.clone()).unwrap_or_else(|err| {
            panic!(
                "Could not make connection pool to {}\nError: {:?}",
                &db_path, err
            )
        });
        database::initialize(&connection_pool).map_err(AccountsRepositoryError::from)?;

        let xand_api = config.create_blockchain(xand_api_client.clone());

        let banks_repository = SqliteBanksRepository::new_boxed(connection_pool.clone());
        let accounts_repository = SqliteAccountsRepository::new_boxed(connection_pool);

        let transaction_repository = config
            .create_transaction_repository(xand_api_client.clone(), accounts_repository.clone());
        dump_fd_info("After create_transaction_repository");

        let member = config
            .create_member(
                xand_api.clone(),
                banks_repository.clone(),
                accounts_repository.clone(),
                transaction_repository.clone(),
            )
            .expect("Could not initialize an instance of Member");
        dump_fd_info("After create member");

        let bank_adapters = Box::new(DefaultBankAdapterFactory {
            secrets: secret_store.clone(),
        });

        Ok(Self {
            config: config.clone(),
            bank_adapters,
            member,
            accounts_repository,
            banks_repository,
        })
    }

    pub fn address(&self) -> &Address {
        self.config.address()
    }

    pub fn add_account(&self, account: Account) -> Result<AccountDetails> {
        self.accounts_repository.insert(account).map_err(Into::into)
    }

    pub fn accounts(&self) -> Result<Vec<AccountDetails>> {
        self.accounts_repository.get_all().map_err(Into::into)
    }

    pub fn account_by_id(&self, id: i32) -> Result<AccountDetails> {
        self.accounts_repository.get(id).map_err(Into::into)
    }

    pub fn update_account(&self, account: Account) -> Result<AccountDetails> {
        self.accounts_repository.update(account).map_err(Into::into)
    }

    pub fn delete_account(&self, id: i32) -> Result<()> {
        self.accounts_repository.remove(id).map_err(Into::into)
    }

    pub async fn bank_balance(&self, id: i32) -> Result<BankAccountBalance> {
        let connected_api = self
            .bank_adapters
            .create_from_account_id(id, self.accounts_repository.clone())?;

        connected_api
            .balance()
            .await
            .map_err(crate::error::Error::from)
    }

    pub fn banks(&self) -> Result<Vec<Bank>> {
        self.banks_repository.get_all().map_err(Into::into)
    }

    pub fn bank(&self, id: i32) -> Result<Bank> {
        self.banks_repository.get(id).map_err(Into::into)
    }

    pub fn add_bank(&self, bank: Bank) -> Result<Bank> {
        self.banks_repository.insert(bank).map_err(Into::into)
    }

    pub fn update_bank(&self, bank: Bank) -> Result<Bank> {
        self.banks_repository.update(bank).map_err(Into::into)
    }

    pub fn delete_bank(&self, id: i32) -> Result<()> {
        self.banks_repository.remove(id).map_err(Into::into)
    }

    pub async fn xand_balance(&self) -> Result<u64> {
        self.member.get_balance().await
    }

    pub async fn transactions(
        &self,
        page_number: Option<usize>,
        page_size: Option<usize>,
    ) -> Result<TransactionHistory> {
        self.member
            .get_transactions(page_number, page_size)
            .await
            .map_err(Into::into)
    }

    pub async fn transaction(&self, id: &str) -> Result<Transaction> {
        self.member
            .get_transaction(id.to_string())
            .await
            .map_err(Into::into)
    }

    pub async fn submit_payment(
        &self,
        receiver: Address,
        amount_in_minor_units: u64,
    ) -> Result<Receipt> {
        self.member
            .send_claims(receiver, amount_in_minor_units)
            .await
            .map_err(Into::into)
    }

    pub async fn issue_create_request(
        &self,
        id: i32,
        amount_in_minor_units: u64,
    ) -> Result<Receipt> {
        self.member
            .issue_create_request(id, amount_in_minor_units)
            .await
            .map_err(Into::into)
    }

    pub async fn redeem(&self, id: i32, amount_in_minor_units: u64) -> Result<Receipt> {
        self.member
            .issue_redeem_request(id, amount_in_minor_units)
            .await
            .map_err(Into::into)
    }

    pub async fn fund_claims(
        &self,
        id: i32,
        correlation_id: &str,
        amount_in_minor_units: u64,
    ) -> Result<FundClaimsResponse> {
        let connected_api = self
            .bank_adapters
            .create_from_account_id(id, self.accounts_repository.clone())?;

        connected_api
            .fund_claims(correlation_id.to_string(), amount_in_minor_units)
            .await
            .map_err(Into::into)
    }

    pub fn services(&self) -> Vec<Box<dyn Service>> {
        self.config.get_services(
            self.bank_adapters.as_ref(),
            self.banks_repository.clone(),
            self.accounts_repository.clone(),
            self.member.xand_api.clone(),
        )
    }
}
