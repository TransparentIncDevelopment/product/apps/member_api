// Invoke these tests with
// `cargo test --features "serializable_config"`

use super::*;

const SAMPLE_YAML_CONFIG: &str = r#"---
path: ~
port: ~
workers: ~
database: "./some/path/to/db"
jwt-secret: ~
xand-api:
  url: "http://fake_validator/"
  auth: ~
  timeout-seconds: ~
secret-store:
  local-file:
    yaml-file-path: ""
address: 5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ"#;

#[cfg(feature = "serializable_config")]
impl MemberApiConfig {
    fn test() -> Self {
        let fake_validator = Url::parse("http://fake_validator").unwrap();
        Self {
            path: None,
            port: None,
            workers: None,
            database: "./some/path/to/db".into(),
            jwt_secret: None,
            xand_api: XandApiConfig {
                url: fake_validator,
                auth: None,
                timeout_seconds: None,
            },
            secret_store: SecretStoreConfig::test(),
            address: Address::from_str("5Hh9Gq21Ns4Knd6CjzjMymK6HeW9yYfxdMfhMoDyA8geHVbJ").unwrap(),
        }
    }
}

#[cfg(feature = "serializable_config")]
impl SecretStoreConfig {
    fn test() -> SecretStoreConfig {
        SecretStoreConfig::LocalFile(LocalFileSecretStoreConfiguration {
            yaml_file_path: String::default(),
        })
    }
}

#[test]
fn can_deserialize_from_yaml_str() {
    // Given sample yaml str
    // Then
    let _deserialized_cfg: MemberApiConfig = serde_yaml::from_str(SAMPLE_YAML_CONFIG).unwrap();
}

#[cfg(feature = "serializable_config")]
#[test]
fn can_serialize_to_yaml_str() {
    insta::assert_yaml_snapshot!(MemberApiConfig::test())
}

#[cfg(feature = "serializable_config")]
#[test]
fn serialize_member_api_config_roundtrip() {
    // Given
    let cfg = MemberApiConfig::test();

    // When
    let serialized_cfg = serde_yaml::to_string(&cfg).unwrap();
    let deserialized_cfg: MemberApiConfig = serde_yaml::from_str(&serialized_cfg).unwrap();

    // Then
    // Smoke test a few fields for equality
    assert_eq!(deserialized_cfg.address, cfg.address);
    assert_eq!(deserialized_cfg.path, cfg.path);
    assert_eq!(deserialized_cfg.database, cfg.database);
    assert_eq!(deserialized_cfg.port, cfg.port);
    assert_eq!(deserialized_cfg.workers, cfg.workers);
}
