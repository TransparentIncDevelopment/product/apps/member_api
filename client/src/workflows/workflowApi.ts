import { TransactionsPollerApi } from "./transactionsPollerApi"
import { CreateApi, CreateRequestParams } from "./create";
import { AccountsApi, BanksApi, MemberApi, Receipt, Transaction, TransactionsApi } from "../api";
import { RedeemApi, RedeemRequest } from "./redeem";
import { PaymentApi, PaymentRequest } from "./paymentApi";

export class WorkflowApi {
    private TransactionsPollerApi: TransactionsPollerApi;
    private createRequestApi: CreateApi;
    private redeemApi: RedeemApi;
    private paymentApi: PaymentApi;
    private TransactionsApi: TransactionsApi;

    constructor(transactionsApi: TransactionsApi, accountsApi: AccountsApi) {
        this.TransactionsApi = transactionsApi;
        this.TransactionsPollerApi = new TransactionsPollerApi(this.TransactionsApi);
        this.redeemApi = new RedeemApi(this.TransactionsApi, this.TransactionsPollerApi);
        this.createRequestApi = new CreateApi(this.TransactionsApi, this.TransactionsPollerApi);
        this.paymentApi = new PaymentApi(this.TransactionsApi, this.TransactionsPollerApi);
    }

    public async createWorkflow(
        createRequestParams: CreateRequestParams): Promise<Transaction> {
        return await this.createRequestApi.createRequestWorkflow(createRequestParams);
    }

    public async createAndWait(
        createRequestReqs: CreateRequestParams): Promise<Receipt> {
        return await this.createRequestApi.createAndWait(createRequestReqs);
    }

    public async redeemWorkflow(redeemRequestReqs: RedeemRequest): Promise<Receipt> {
        return await this.redeemApi.redeemRequestWorkflow(redeemRequestReqs);
    }

    public async redeemAndWait(redeemRequestReqs: RedeemRequest): Promise<Receipt> {
        return await this.redeemApi.redeemAndWait(redeemRequestReqs);
    }

    public async payAndWait(request: PaymentRequest): Promise<Receipt> {
        return await this.paymentApi.payAndWait(request);
    }
}