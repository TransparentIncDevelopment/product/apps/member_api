import {
    CreateRequest,
    Receipt,
    Transaction, TransactionsApi,
} from "../api";
import { TransactionsPollerApi } from "./transactionsPollerApi";
import { WorkflowError, ErrorType } from "./errors";
import { Retrier } from "./retrier";

export class CreateApi {
    private transactionsApi: TransactionsApi;
    private TransactionsPollerApi: TransactionsPollerApi;

    constructor(transactionsApi: TransactionsApi, TransactionsPollerApi: TransactionsPollerApi) {
        this.TransactionsPollerApi = TransactionsPollerApi;
        this.transactionsApi = transactionsApi;
    }

    public async createRequestWorkflow(request: CreateRequestParams): Promise<Transaction> {
        const createRequest : CreateRequest = {
            accountId: request.accountId,
            amountInMinorUnit: request.amount
        };
        let createResponse: Receipt;

        try {
            createResponse = (await this.transactionsApi.createRequestTransaction(createRequest)).data;
        } catch (error) {
            throw new WorkflowError(ErrorType.createRequestFailed, error);
        }

        try {
            await this.transactionsApi.fundClaimsTransaction(createResponse.transactionId, {
                correlationId: createResponse.correlationId!,
                amountInMinorUnit: request.amount,
                bankAccountId: request.accountId
            });
        } catch (error) {
            throw new WorkflowError(ErrorType.transferToClaimsAccountFailed, error);
        }

        return await this.TransactionsPollerApi.awaitTransactionConfirmation({
            transactionId: createResponse.transactionId,
            retrier: request.retrier
        });
    }

    public async createAndWait(request: CreateRequestParams): Promise<Receipt> {
        let receipt: Receipt;
        try {
            receipt = (await this.transactionsApi.createRequestTransaction({
                accountId: request.accountId,
                amountInMinorUnit: request.amount
            })).data;
        }
        catch(error) {
            throw new WorkflowError(ErrorType.createRequestFailed, error);
        }
        
        await this.TransactionsPollerApi.awaitTransaction({
            transactionId: receipt.transactionId,
            retrier: request.retrier
        });

        return receipt;
    }
}

export type CreateRequestParams = {
    accountId: number;
    amount: number;
    retrier?: Retrier;
}
