import {CreateTestContext} from "./createTestContext"
import { ErrorType } from "../errors";


describe("Given a create api", () => {
    let context: CreateTestContext;

    beforeEach(() => {
        context = new CreateTestContext();
    });

    describe("Given a create request workflow for $5.24", () => {

        beforeEach(() => {
            context.setClaimsAcctTransferAmtInMinorUnit(524);
        })


        describe("And a request that will always be confirmed", () => {
            beforeEach(async () => {
                await context
                    .setCreateTransactionConfirmed()
                    .setupCreateRequest()
                    .setupPollingReturnsFinalizedTransaction()
                    .initiateCreateWorkflow();
            });
            describe("When the create request api accepts the create request", () => {
                it("Then a transfer from bank account to claims account for 524 cents is made", () => {
                    context.thenBankTransferWasSubmitted();
                });
                it("Then it returns a finalized transaction", () => {
                    context.thenTransactionIsFinalized();
                });
            });
        });

        describe("And a request transaction that will always be pending", () => {
            beforeEach(async () => {
                await context
                    .setCreateTransactionPending()
                    .setupCreateRequest()
                    .setupPollingTimesOut()
                    .initiateCreateWorkflow();
            });

            describe("When the create request api accepts the create request", () => {
                it("Then it returns a descriptive polling timeout error", () => {
                    context.thenErrorReturnedIs(ErrorType.pollingTimeoutError);
                });
            });
        });

        describe("And a request that uses a polling duration " +
            "sufficient to finalize a transaction", () => {
            beforeEach(async () => {
                await context
                    .setCreateTransactionConfirmed()
                    .setupCreateRequest()
                    .setupPollingReturnsFinalizedTransaction()
                    .initiateCreateWorkflow();
            });
            describe("When the create request api accepts the create request", () => {
                it("Then it returns a finalized transaction", () => {
                    context.thenTransactionIsFinalized();
                });
            });

        });

        describe("And that request that uses a polling duration insufficient to finalize a transaction", () => {
            beforeEach(async () => {
                await context
                    .setCreateTransactionPending()
                    .setupCreateRequest()
                    .setupPollingTimesOut()
                    .initiateCreateWorkflow();
            });
            describe("When the create request api accepts the create request", () => {
                it("Then it returns a descriptive polling timeout error", () => {
                    context.thenErrorReturnedIs(ErrorType.pollingTimeoutError);
                });
            });
        });

        describe("And the request is cancelled after it is submitted", () => {
            beforeEach(async () => {
                await context
                    .setCreateTransactionCancelled()
                    .setupCreateRequest()
                    .setupPollingReturnsCancelledTransaction()
                    .initiateCreateWorkflow();
            });
            describe("When the create request api accepts the create request", () => {
                it("Then it returns a descriptive cancellation error", () => {
                    context.thenErrorReturnedIs(ErrorType.cancelledTransactionError);
                });
            });
        });

        describe("And a request that will be determined to be invalid", () => {
            beforeEach(async () => {
                await context
                    .setCreateTransactionInvalid()
                    .setupCreateRequest()
                    .setupPollingReturnsCancelledTransaction()
                    .initiateCreateWorkflow();
            });
            describe("When the create request api accepts the create request", () => {
                it("Then it returns a descriptive cancellation error", () => {
                    context.thenErrorReturnedIs(ErrorType.cancelledTransactionError);
                });
            });
        });

        describe("And a bad create request", () => {
            beforeEach(async () => {
                await context
                    .setupCreateRequestThrowsError()
                    .initiateCreateWorkflow();
            });
            describe("When the create request api accepts the create request", () => {
                it("Then it returns a descriptive create request failure error", () => {
                    context.thenErrorReturnedIs(ErrorType.createRequestFailed);
                });
            });
        });

        describe("And a claims account transfer that cannot be completed", () => {
            beforeEach(async () => {
                await context
                    .setClaimsAcctTransferAmtInMinorUnit(111)
                    .setCreateTransactionPending()
                    .setupCreateRequest()
                    .setupClaimsAcctTransferThrowsError()
                    .initiateCreateWorkflow();
            });
            describe("When the create request api accepts the create request", () => {
                it("Then it returns a descriptive claim funding failure error", () => {
                    context.thenErrorReturnedIs(ErrorType.transferToClaimsAccountFailed);
                });
            });
        });
    });

    describe("And good create request", () => {
        beforeEach(async () => {
            await context
                .setCreateTransactionConfirmed()
                .setupCreateRequest()
                .setupPollingReturnsFinalizedTransaction()
                .initiateCreateWorkflow();
        });

        
    });
});
