import {RedeemTestContext} from "./redeemTestContext";

describe("Given a Redeem api", () => {
    let context: RedeemTestContext;

    beforeEach(() => {
        context = new RedeemTestContext();

    });

    describe("Given the wallet api accepts the redeem request", () => {
        beforeEach(() => {
            context.givenRedeemRequestAccepted()
        });
        describe("When I execute a redeem request workflow", () => {
            beforeEach(async () => {
                await context.redeemWorkflow()
            });
            it("Then a receipt is returned", () => {
                context.thenReceiptIsReturned();
            });
        });

        describe("Given polling fails with error", () => {
            beforeEach(() => {
                context.givenPollingFailsWithTimeoutError();
            });
            describe("When I execute a redeem request workflow", () => {
                beforeEach(async () => {
                    await context.redeemWorkflow()
                });

                it("Then polling error is returned", () => {
                    context.thenPollingErrorReturned();
                });
            });
        });
    });

    describe("Given the wallet API rejects the redeem request", () => {
        beforeEach(() => {
            context.givenRedeemRequestRejected();
        })

        describe("When I execute a redeem request workflow", () => {
            beforeEach(async () => {
                await context.redeemWorkflow()
            });

            it("Then redeem request error is returned", () => {
                context.thenRedeempRequestErrorReturned();
            });
        });
    })
});
