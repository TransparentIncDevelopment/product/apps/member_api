import { RedeemApi } from "../redeem";
import { TransactionsPollerApi } from "../transactionsPollerApi";
import { Receipt, MemberApi, TransactionsApi } from "../../api";
// @ts-ignore
import td from "testdouble";
import { expect } from "chai";
import { WorkflowError, ErrorType } from "../errors";
import { Retrier } from "../retrier";

export class RedeemTestContext {
  private transactionsApi: TransactionsApi;
  private redeemApi: RedeemApi;
  private pollerApi: TransactionsPollerApi;

  private amount = 567;
  private xandAddress = "the_xand_addr";
  private bankAccount = 1234566;
  private retrier: Retrier = new Retrier(1, 1);
  private transactionId = "123456";
  private correlationId = "the_correlationId";

  private returnedReceipt: Receipt | null;
  private fakeReceipt: Receipt | null;
  private returnedError: WorkflowError | null;
  private fakePollingError: WorkflowError | null;
  private fakeRedeemError: Error | null;

  constructor() {
    this.pollerApi = td.object<TransactionsPollerApi>();
    this.transactionsApi = td.object<TransactionsApi>();
    this.redeemApi = new RedeemApi(this.transactionsApi, this.pollerApi);

    this.returnedReceipt = null;
    this.fakeReceipt = null;
    this.returnedError = null;
    this.fakePollingError = null;
    this.fakeRedeemError = null;
  }

  public async redeemWorkflow() {
    try {
      this.returnedReceipt = await this.redeemApi.redeemRequestWorkflow({
        accountId: this.bankAccount,
        address: this.xandAddress,
        amount: this.amount,
        retrier: this.retrier,
      });
    } catch (error) {
      this.returnedError = error;
    }
  }

  public thenReceiptIsReturned() {
    expect(this.returnedReceipt).to.deep.equal(this.fakeReceipt);
  }

  public givenRedeemRequestAccepted() {
    this.fakeReceipt = { correlationId: this.correlationId, transactionId: this.transactionId };
    td.when(
      this.transactionsApi.redeemRequestTransaction({
        accountId: this.bankAccount,
        amountInMinorUnit: this.amount,
      })
    ).thenResolve({ data: this.fakeReceipt });
  }

  public givenPollingFailsWithTimeoutError() {
    this.fakePollingError = new WorkflowError(
      ErrorType.pollingTimeoutError,
      Error("Some error message")
    );

    td.when(
      this.pollerApi.awaitTransactionConfirmation({
        transactionId: this.transactionId,
        retrier: this.retrier,
      })
    ).thenReject(this.fakePollingError);
  }

  public thenPollingErrorReturned() {
    expect(this.returnedError).to.equal(this.fakePollingError);
  }

  public givenRedeemRequestRejected() {
    this.fakeRedeemError = new Error("Some client api error");
    td.when(
      this.transactionsApi.redeemRequestTransaction({
        accountId: this.bankAccount,
        amountInMinorUnit: this.amount,
      })
    ).thenReject(this.fakeRedeemError);
  }

  public thenRedeempRequestErrorReturned() {
    expect(this.returnedError!.errorType).to.equal(
      ErrorType.redeemRequestFailed
    );
    expect(this.returnedError!.message).to.equal(this.fakeRedeemError!.message);
  }
}
