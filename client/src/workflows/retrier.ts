export function wait(time: number) {
    return new Promise((resolve) => setTimeout(() => resolve(time), time));
}

export class Retrier {
    private retries: number = 100;
    private delay: number = 500;

    constructor(retries: number = 100, delay: number = 500) {
        this.retries = retries;
        this.delay = delay;
    }

    public async execute<T>(fn: () => Promise<T>): Promise<T> {
        const retries = this.retries;
        return fn()
            .catch((err: any) => {
                if (retries > 1)
                    return wait(this.delay).then(() => this.retry(fn, retries - 1))
                else
                    throw err;
            }
            );
    }

    private async retry<T>(fn: () => Promise<T>, retries: number): Promise<T> {
        return fn()
            .catch((err: any) => {
                if (retries > 1)
                    return wait(this.delay).then(() => this.retry(fn, retries - 1));
                else
                    throw err;
            });
    }
}
