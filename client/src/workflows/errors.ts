enum ErrorType {
    cancelledTransactionError = "Cancelled Transaction",
    createRequestFailed = "Create Request Failed",
    failedTransaction = "Failed Transaction",
    fetchingTransactionFailed = "Error while getting transaction (GetTransactionById)",
    pollingTimeoutError = "Polling Timeout",
    redeemRequestFailed = "Error while submitting redeem request",
    transferToClaimsAccountFailed = "Transfer To claims account failed",
    paymentRejected = "Payment rejected"
}

class WorkflowError extends Error {
    // tslint:disable-next-line:variable-name
    public errorType: ErrorType;
    constructor(errorType: ErrorType, error: Error) {
        super(error.message);
        this.errorType = errorType;
        // tslint:disable-next-line:max-line-length
        // Set a new prototype for custom error to preserve chain of inheritance from this class due to known TS issue https://github.com/Microsoft/TypeScript-wiki/blob/master/Breaking-Changes.md#extending-built-ins-like-error-array-and-map-may-no-longer-work
        const newProto = new.target.prototype;
        Object.setPrototypeOf(this, newProto);
    }
}

export { WorkflowError, ErrorType };
