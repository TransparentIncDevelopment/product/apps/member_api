include:
- file: /templates/rust/gitlab-ci.definitions.yaml
  project: transparentincdevelopment/product/devops/tpfs-ci
  ref: v1.3.1


stages:
  - build
  - test
  - lint
  - publish

# GitLab feature will kick off "detached" pipelines for MergeRequests for conditional or manual jobs (like `publish-member-api-crate` below)
# Explicitly disable "Merge Request pipelines" and only allow branch and tag pipelines
# Rules pulled from examples here: https://docs.gitlab.com/ee/ci/yaml/README.html#workflowrules
workflow:
  rules:
      # Control when merge request pipelines run.
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
      when: never
      # Control when both branch pipelines and tag pipelines run.
    - if: '$CI_PIPELINE_SOURCE == "push"'
      when: always
      # Enable kicking off pipelines manually from gitlab web ui
    - if: '$CI_PIPELINE_SOURCE == "web"'
      when: always



.sticky-vm-variables:
  &sticky-vm-variables # Naming convention: Partition builds to go to same location per project per branch
  GIT_CLONE_PATH: "$CI_BUILDS_DIR/$CI_PROJECT_ID/$CI_COMMIT_REF_SLUG/$CI_PROJECT_NAME"

  # Git clean everything except the /target directory
  GIT_CLEAN_FLAGS: -ffdx -e /target

  # Naming convention: Maintain a global CARGO_HOME so downloaded dependencies are re-used across the build VM
  CARGO_HOME: "$CI_BUILDS_DIR/$CI_PROJECT_ID/cargohome"

## Docker images used across jobs. Keep TPFS images in sync / reference the latest docker image available in the
## /docker/.env file
variables:
  CI_IMG: "gcr.io/xand-dev/member-api-ci:1.2.0"

##
## Helper templates
##
.create-artifactory-creds: &create-artifactory-creds
  - echo "registry \"$ARTIFACTORY_URL/api/npm/npm\"" > ~/.yarnrc
  - echo "registry=$ARTIFACTORY_URL/api/npm/npm" > ~/.npmrc
  - curl -u$ARTIFACTORY_USER:$ARTIFACTORY_PASS $ARTIFACTORY_URL/api/npm/auth >> ~/.npmrc

.authorize-gcloud:
  - echo $GCLOUD_DEV_SERVICE_KEY | base64 -id > gcloud-service-key.json
  - gcloud auth activate-service-account --key-file gcloud-service-key.json

.setup-crates-ssh-agent: &setup-crates-ssh-agent
  - eval `ssh-agent`
  - chmod 0600 $DEPLOY_SSH_KEY
  - ssh-add $DEPLOY_SSH_KEY

################################################################################
build-member-api:
  image: $CI_IMG
  # extends: .build-template
  stage: build
  variables:
    <<: *sticky-vm-variables
  tags:
    - sticky-docker-runner
  interruptible: true
  script:
    - *setup-crates-ssh-agent
    - cargo build --release

build-member-api-config:
  image: $CI_IMG
  # extends: .build-template
  stage: build
  variables:
    <<: *sticky-vm-variables
  tags:
    - sticky-docker-runner
  interruptible: true
  script:
    - *setup-crates-ssh-agent
    - cd member-api-config
    - cargo build --release --features "serializable_config" # Ensure can build config with "serializable_config" feature

test-member-api:
  # extends: .test-template
  image: $CI_IMG
  variables:
    <<: *sticky-vm-variables
  tags:
    - sticky-docker-runner
  interruptible: true
  script:
    - *setup-crates-ssh-agent
    - cargo test --release --all-features
    - cd .docker
    - make artifacts
  artifacts:
    paths:
      - "$CI_PROJECT_DIR/.docker/tmp"

lint-member-api:
  # extends: .lint-template
  image: $CI_IMG
  stage: lint
  variables:
    <<: *sticky-vm-variables
    # Disable `git fetch` for jobs running in parallel (same stage) to avoid .git/ conflict on sticky VM
    GIT_STRATEGY: "none"
  tags:
    - sticky-docker-runner
  interruptible: true
  script:
    - *setup-crates-ssh-agent
    - cargo fmt --all -- --check
    - cargo clippy --all-targets --all-features --release -- -D warnings
    - ./.ci/assert-unmodified-child-crate-versions.sh

audit-member-api:
  image: $CI_IMG
  variables:
    <<: *sticky-vm-variables
    # Disable `git fetch` for jobs running in parallel (same stage) to avoid .git/ conflict on sticky VM
    GIT_STRATEGY: "none"
  tags:
    - sticky-docker-runner
  interruptible: true
  script:
    - *setup-crates-ssh-agent
    - cargo audit

.publish-member-api-beta:
  stage: publish
  image: $CI_IMG
  tags:
    - kubernetes-runner
  script:
    - *setup-crates-ssh-agent
    # TPFS_CRATES_TOKEN is defined as GitLab group level variable: https://gitlab.com/groups/TransparentIncDevelopment/-/settings/ci_cd
    - cargo login --registry tpfs $TPFS_CRATES_TOKEN
    - ./.ci/set-beta-version.sh $CI_PIPELINE_ID
    - ./.ci/sync-intra-repo-versions.sh
    - ./.ci/echo-toml-diffs.sh
    - cargo publish --registry tpfs --allow-dirty --manifest-path ./member-api-config/Cargo.toml
    - cargo publish --registry tpfs --allow-dirty --manifest-path ./member-api/Cargo.toml

# Make a manual publish job available when not on master or a tag
publish-member-api-crate-beta:
  extends: .publish-member-api-beta
  when: manual
  except:
    - master
    - tags
# Always publish beta on merge to master
publish-member-api-mainline:
  extends: .publish-member-api-beta
  only:
    refs:
      - master

publish-member-api-crate:
  stage: publish
  image: $CI_IMG
  only:
    - tags
  except:
    - branches
  tags:
    - kubernetes-runner
  script:
    - *setup-crates-ssh-agent
    # TPFS_CRATES_TOKEN is defined as GitLab group level variable: https://gitlab.com/groups/TransparentIncDevelopment/-/settings/ci_cd
    - cargo login --registry tpfs $TPFS_CRATES_TOKEN
    - ./.ci/sync-intra-repo-versions.sh
    - ./.ci/echo-toml-diffs.sh
    - ./.ci/verify-crate-versions-match.sh $CI_COMMIT_TAG
    - cargo publish --registry tpfs --allow-dirty --manifest-path ./member-api-config/Cargo.toml
    - cargo publish --registry tpfs --allow-dirty --manifest-path ./member-api/Cargo.toml

### DOCKER
.publish-docker:
  stage: publish
  image: $CI_IMG
  variables:
    DOCKER_DRIVER: overlay2
    DOCKER_HOST: tcp://localhost:2375
  services:
    - docker:18.09-dind
  tags:
    - kubernetes-runner
  # Gets artifacts at .docker/tmp/
  needs:
    - job: test-member-api
      artifacts: true
    - job: lint-member-api
  before_script:
    - docker login -u _json_key -p "$(echo $GCLOUD_DEV_SERVICE_KEY | base64 -d)" https://gcr.io
  script:
    - cd .docker
    - make debug-gitlab
    - make build
    - make publish

# Make a manual publish job available when not on master or a tag
publish-docker-beta:
  extends: .publish-docker
  when: manual
  except:
    - master
    - tags

# Always publish an image on merge to master
publish-docker-mainline:
  extends: .publish-docker
  only:
    refs:
      - master

# Tag-to-publish flow
publish-docker-tag:
  extends: .publish-docker
  only:
    - tags
  except:
    - branches

### K8s
.publish-k8s-package:
  image: $CI_IMG
  tags:
    - kubernetes-runner
  stage: publish
  script:
    - cd ./.k8s
    - make clean package
    - make publish-artifactory

# Make a manual publish job available when not on master or a tag
publish-k8s-beta:
  extends: .publish-k8s-package
  when: manual
  except:
    - master
    - tags

# Always publish on merge to master
publish-k8s-mainline:
  extends: .publish-k8s-package
  only:
    refs:
      - master

# Tag-to-publish flow
publish-k8s-tag:
  extends: .publish-k8s-package
  only:
    - tags
  except:
    - branches

### Member API Client
.node-lts-config:
  image: $CI_IMG
  tags:
    - kubernetes-runner

.node-modules-cache:
  cache:
    key: ${CI_COMMIT_REF_SLUG}
    paths:
      - node_modules/
      - yarn.lock

test-api-client:
  stage: test
  extends:
    - .node-lts-config
    - .node-modules-cache
  before_script:
    - *create-artifactory-creds
  script:
    - cd client
    - yarn install
    - yarn build
    - yarn test

lint-api-client:
  stage: lint
  extends:
    - .node-lts-config
    - .node-modules-cache
  script:
    - cd client
    - yarn lint

build-api-client:
  stage: build
  extends:
    - .node-lts-config
    - .node-modules-cache
  before_script:
    - *create-artifactory-creds
  script:
    - cd client
    - yarn install
    - yarn build

.publish-api-client:
  stage: publish
  extends:
    - .node-lts-config
    - .node-modules-cache
  before_script:
    - *create-artifactory-creds
  script:
    - cd client
    - make build publish
  artifacts:
    name: ${CI_COMMIT_REF_SLUG}
    paths:
      - client/dist/

# Make a manual beta publish job available when not on master or a tag
publish-member-api-client-beta:
  extends: .publish-api-client
  when: manual
  except:
    - master
    - tags

# Always publish on merge to master
publish-member-api-client-mainline:
  extends: .publish-api-client
  only:
    refs:
      - master

# Tag-to-publish flow
publish-member-api-client:
  extends: .publish-api-client
  only:
    - tags
  except:
    - branches

sync-dependencies:
  extends: .synchronize-central-dependency-registry

upgrade-downstream-repos:
  extends: .upgrade-consuming-repos
  variables:
    UPSTREAM_CRATES: member-api-config member-api member-api-server
  before_script:
    - export REPO_VERSION=$(toml get ./member-api/Cargo.toml package.version | tr -d \")
